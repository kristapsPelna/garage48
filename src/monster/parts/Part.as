package monster.parts 
{
	import flash.geom.Point;
	import mechanics.Weapon;
	import monster.Animation;
	import monster.DragonBoneTest;
	import objects.Box;
	import starling.animation.DelayedCall;
	import starling.core.Starling;
	
	/**
	 * Ķermeņa daļām pamatklase
	 * @author Kamazs
	 */
	public class Part extends Box 	{
		
		public var race:int;
		public var difficultyRating:int;
		public var currentMode:String = null;
		
		private var delayCall:DelayedCall;
		
		public var weapon:Weapon;
		
		public function Part() 		{
			super();
			group = "boss";
			explosionType = "boss";
		}
		
		public function setMode(modeName:String):void {
			
			if (currentMode == modeName) {
				return;
			}
			if (animation == null) {
				return;
			}
			if (animation.parent == null) {
				addChild(animation);
			}
			currentMode = modeName;
			animation.play(modeName);
		}
		
		public function shoot(location:Point):void
		{
			var success:Boolean = false;
			if (weapon)
				success = weapon.shoot(location);
				
			if (success && (this is LeftArm || this is RightArm))
			{
				animation.play(DragonBoneTest.stateAttack);
				if (delayCall)
					delayCall.reset(resetAnim, 0.25);
				else
					delayCall = Starling.juggler.delayCall(resetAnim, 0.25) as DelayedCall;
			}
		}
		
		private function resetAnim():void 
		{
			animation.play(DragonBoneTest.stateIdle);
		}
		
		override public function destroy():void 
		{
			if (weapon != null) {
				weapon.owner = null;
				weapon = null;
			}
			
			super.destroy();
			animation.removeFromParent(true);
			animation = null;
		}
		
	}

}