package monster.parts 
{
	import monster.Animation;
	import monster.DragonBoneTest;
	import monster.TmpAnimation;
	import objects.Box;
	
	/**
	 * BOSA ķermenis
	 * @author Kamazs
	 */
	public class Body extends Part 	{
		
		public function Body()		{
			super();
			
			animation = new Animation( DragonBoneTest.torso, Constants.random.nextMinMax(0, DragonBoneTest.totalVariations) );// new TmpAnimation("body") );
		}
		
	}

}