package monster.parts
{
	import mechanics.Arsenal;
	import monster.Animation;
	import monster.DragonBoneTest;
	
	/**
	 * Bosa labā roka
	 * @author Kamazs
	 */
	public class RightArm extends Part
	{
		
		public function RightArm()
		{
			super();
			
			animation = new Animation( DragonBoneTest.handLeft, Constants.random.nextMinMax(0, DragonBoneTest.totalVariations));
			
			//weapon = Arsenal.getRandomWeapon();
			//weapon.owner = this;
		}
	
	}

}