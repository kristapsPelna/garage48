package monster.parts
{
	import mechanics.Arsenal;
	import monster.Animation;
	import monster.DragonBoneTest;
	
	/**
	 * Bosa kreisā roka
	 * @author Kamazs
	 */
	public class LeftArm extends Part
	{
		
		public function LeftArm()
		{
			super();
			
			animation = new Animation(  DragonBoneTest.handRight, Constants.random.nextMinMax(0, DragonBoneTest.totalVariations) );
			
			//weapon = Arsenal.getRandomWeapon();
			//weapon.owner = this;
		}
	
	}

}