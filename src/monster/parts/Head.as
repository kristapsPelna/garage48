package monster.parts 
{
	import monster.Animation;
	import monster.DragonBoneTest;
	import monster.TmpAnimation;
	import objects.Box;
	
	/**
	 * Bosa galva
	 * @author Kamazs
	 */
	public class Head extends Part 	{
		
		public function Head() 		{
			super();
			
			animation = new Animation( DragonBoneTest.head, Constants.random.nextMinMax(0, DragonBoneTest.totalVariations) );
		}
		
	}

}