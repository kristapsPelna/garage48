package monster.parts 
{
	import monster.Animation;
	import monster.DragonBoneTest;
	import monster.TmpAnimation;
	import objects.Box;
	
	/**
	 * Bosa kājas
	 * @author Kamazs
	 */
	public class Legs extends Part 	{
		
		public function Legs() 		{
			super();
			
			animation = new Animation( DragonBoneTest.legs, Constants.random.nextMinMax(0, DragonBoneTest.totalVariations) );
		}
		
	}

}