package monster.behaviours
{
	import monster.AI;
	import monster.DragonBoneTest;
	
	/**
	 * ...
	 * @author Ace
	 */
	public class Standing extends AI
	{
		public static const huntUpdateInterval:int = 1; //seconds
		
		public function Standing()
		{
			name = "standing";
		}
		
		override public function onUpdate(timeSinceLastUpdate:int):Boolean
		{
			if (boss.legs)
				boss.legs.setMode(DragonBoneTest.stateIdle);
				//boss.animation.play(DragonBoneTest.stateIdle);
			
			return true;
		}
	
	}

}