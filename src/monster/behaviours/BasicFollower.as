package monster.behaviours 
{
	import monster.AI;
	import monster.Boss;
	import monster.DragonBoneTest;
	import objects.Box;
	import objects.Character;
	
	/**
	 * ...
	 * @author Kamazs
	 */
	public class BasicFollower extends AI 
	{
		public static const huntUpdateInterval:int = 1//1000; // sekundes
		public function BasicFollower() {
			super();
			name = "follow";
		}
		
		override public function onUpdate(timeSinceLastUpdate:int):Boolean {
			if (timeSinceLastUpdate > huntUpdateInterval) {
				//hunt player
				if (Character.instance != null) {
					boss.moveTowardsBox(Character.instance); //<- trygonometric approach
					boss.legs.setMode(DragonBoneTest.stateWalk);
					/*var pc:Character = Character.instance;
					var tX:Number = pc.x + pc.width / 2;
					var tY:Number = pc.y + pc.height;
					
					var deltaX:Number = tX - x;
					var dirX:int = -int(deltaX / Math.abs(deltaX));
					
					var deltaY:Number = tY - y;
					var dirY:int = -int(deltaY / Math.abs(deltaY));	
					
					moveInDirection(dirX, dirY);*/
				}				
				
				return true;
			} else {
				return false;
			}
		}
		
	}

}