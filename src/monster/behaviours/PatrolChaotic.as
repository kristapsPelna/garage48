package monster.behaviours 
{
	import flash.geom.Point;
	import monster.AI;
	import monster.DragonBoneTest;
	/**
	 * ...
	 * @author Ace
	 */
	public class PatrolChaotic extends AI
	{
		public static const huntUpdateInterval:int = 1;
		
		private static const pattern:Vector.<Point> = new <Point>[
			new Point(Constants.stageW * 0.2, Constants.stageH * 0.2),
			new Point(Constants.stageW * 0.8, Constants.stageH * 0.2),
			new Point(Constants.stageW * 0.2, Constants.stageH * 0.8),
			new Point(Constants.stageW * 0.8, Constants.stageH * 0.8),
			
			new Point(Constants.stageW * 0.5, Constants.stageH * 0.2),
			new Point(Constants.stageW * 0.8, Constants.stageH * 0.5),
			new Point(Constants.stageW * 0.2, Constants.stageH * 0.5),
			new Point(Constants.stageW * 0.5, Constants.stageH * 0.8),
			
			new Point(Constants.stageW * 0.33, Constants.stageH * 0.5),
			new Point(Constants.stageW * 0.66, Constants.stageH * 0.5),
			new Point(Constants.stageW * 0.5, Constants.stageH * 0.33),
			new Point(Constants.stageW * 0.5, Constants.stageH * 0.55)
		];
		
		private static var currentPoint:Point;
		
		public function PatrolChaotic() 
		{
			name = "patrolChaotic";
		}
		
		override public function onUpdate(timeSinceLastUpdate:int):Boolean
		{
			if (!currentPoint)
				getRandomPoint();
			
			if (timeSinceLastUpdate > huntUpdateInterval) {
				boss.moveTowards(currentPoint.x, currentPoint.y); //<- trygonometric approach
				boss.legs.setMode(DragonBoneTest.stateWalk);
				
				if (Math.abs(boss.x - currentPoint.x) + Math.abs(boss.y - currentPoint.y) < 70)
					getRandomPoint();
				
				return true;
			} else {
				return false;
			}
		}
		
		private function getRandomPoint():void 
		{
			currentPoint = pattern[Constants.random.nextMinMax(0, pattern.length)];
			currentPoint.x -= boss.collisionBox.width * 0.5;
			currentPoint.y -= boss.collisionBox.height * 0.5;
		}
		
	}

}