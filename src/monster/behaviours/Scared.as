package monster.behaviours
{
	import monster.AI;
	import monster.DragonBoneTest;
	import objects.Character;
	
	/**
	 * ...
	 * @author Ace
	 */
	public class Scared extends AI
	{
		public static const huntUpdateInterval:int = 1; //seconds
		
		public function Scared()
		{
			name = "run";
		}
		
		override public function onUpdate(timeSinceLastUpdate:int):Boolean
		{
			if (timeSinceLastUpdate > huntUpdateInterval)
			{
				//hunt player
				if (!Character.instance)
					return true;
				
				boss.moveAwayFromBox(Character.instance); //<- trygonometric approach
				boss.legs.setMode(DragonBoneTest.stateWalk);
				/*var pc:Character = Character.instance;
				   var tX:Number = pc.x + pc.width / 2;
				   var tY:Number = pc.y + pc.height;
				
				   var deltaX:Number = tX - x;
				   var dirX:int = -int(deltaX / Math.abs(deltaX));
				
				   var deltaY:Number = tY - y;
				   var dirY:int = -int(deltaY / Math.abs(deltaY));
				
				 moveInDirection(dirX, dirY);*/
				
				return true;
			}
			else
			{
				return false;
			}
		}
	
	}

}