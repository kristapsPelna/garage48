package monster 
{
	import starling.display.Sprite;
	/**
	 * ...
	 * @author Ace
	 */
	public class Animation extends Sprite {
		private var dragonBone:DragonBoneTest;
		private var db:DragonBoneTest;
		
		public function Animation(type:String, id:int) {
			
			if (id >= 0) {
				db = new DragonBoneTest(type, id);
				addChild(db);
			} 
		}
		
		public function play(animName:String):void {
			db.play(animName);
		}
		
	}

}