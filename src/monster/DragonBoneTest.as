package monster 
{
	import dragonBones.animation.WorldClock;
	import dragonBones.Armature;
	import dragonBones.events.AnimationEvent;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.XMLDataParser;
	import dragonBones.textures.StarlingTextureAtlas;
	import flash.geom.Point;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author Ace
	 */
	public class DragonBoneTest extends Sprite
	{
		private static var factoryArr:Array = [];
		private static var textureArr:Array = [];
		private static var skeletonArr:Array = [];
		
		public static const stateIdle:String = "idle";
		public static const stateAttack:String = "attack";
		public static const stateWalk:String = "walk";
		
		public static const head:String = "head_devil";
		public static const handRight:String = "hand_devil_right";
		public static const legs:String = "legs_devil";
		public static const torso:String = "torso_devil";
		public static const handLeft:String = "hand_devil_left";
		
		public static const totalVariations:int = 4;
		
		//hand right
		[Embed(source = "/../assets/Boss/Devil/hand_devil_right/texture.png", mimeType = "image/png")]
		public static const textureImghand_devil_right0:Class;
		
		[Embed(source = "/../assets/Boss/Devil/hand_devil_right/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLhand_devil_right0:Class;
		
		[Embed(source = "/../assets/Boss/Devil/hand_devil_right/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLhand_devil_right0:Class;
		
		//head
		[Embed(source = "/../assets/Boss/Devil/head_devil/texture.png", mimeType = "image/png")]
		public static const textureImghead_devil0:Class;
		
		[Embed(source = "/../assets/Boss/Devil/head_devil/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLhead_devil0:Class;
		
		[Embed(source = "/../assets/Boss/Devil/head_devil/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLhead_devil0:Class;
		
		//legs_devil
		[Embed(source = "/../assets/Boss/Devil/legs_devil/texture.png", mimeType = "image/png")]
		public static const textureImglegs_devil0:Class;
		
		[Embed(source = "/../assets/Boss/Devil/legs_devil/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLlegs_devil0:Class;
		
		[Embed(source = "/../assets/Boss/Devil/legs_devil/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLlegs_devil0:Class;
		
		//torso_devil
		[Embed(source = "/../assets/Boss/Devil/torso_devil/texture.png", mimeType = "image/png")]
		public static const textureImgtorso_devil0:Class;
		
		[Embed(source = "/../assets/Boss/Devil/torso_devil/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLtorso_devil0:Class;
		
		[Embed(source = "/../assets/Boss/Devil/torso_devil/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLtorso_devil0:Class;
		
		//hand_devil_left
		[Embed(source = "/../assets/Boss/Devil/hand_devil_left/texture.png", mimeType = "image/png")]
		public static const textureImghand_devil_left0:Class;
		
		[Embed(source = "/../assets/Boss/Devil/hand_devil_left/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLhand_devil_left0:Class;
		
		[Embed(source = "/../assets/Boss/Devil/hand_devil_left/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLhand_devil_left0:Class;
		
		//SKELETON MONSTER
		
		//torso_skeleton
		[Embed(source = "/../assets/Boss/Skeleton/torso_skeleton/texture.png", mimeType = "image/png")]
		public static const textureImgtorso_devil1:Class;
		
		[Embed(source = "/../assets/Boss/Skeleton/torso_skeleton/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLtorso_devil1:Class;
		
		[Embed(source = "/../assets/Boss/Skeleton/torso_skeleton/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLtorso_devil1:Class;
		
		//legs_skeleton
		[Embed(source = "/../assets/Boss/Skeleton/legs_skeleton/texture.png", mimeType = "image/png")]
		public static const textureImglegs_devil1:Class;
		
		[Embed(source = "/../assets/Boss/Skeleton/legs_skeleton/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLlegs_devil1:Class;
		
		[Embed(source = "/../assets/Boss/Skeleton/legs_skeleton/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLlegs_devil1:Class;
		
		//head_skeleton
		[Embed(source = "/../assets/Boss/Skeleton/head_skeleton/texture.png", mimeType = "image/png")]
		public static const textureImghead_devil1:Class;
		
		[Embed(source = "/../assets/Boss/Skeleton/head_skeleton/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLhead_devil1:Class;
		
		[Embed(source = "/../assets/Boss/Skeleton/head_skeleton/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLhead_devil1:Class;
		
		//hand_skeleton_right
		[Embed(source = "/../assets/Boss/Skeleton/hand_skeleton_right/texture.png", mimeType = "image/png")]
		public static const textureImghand_devil_right1:Class;
		
		[Embed(source = "/../assets/Boss/Skeleton/hand_skeleton_right/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLhand_devil_right1:Class;
		
		[Embed(source = "/../assets/Boss/Skeleton/hand_skeleton_right/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLhand_devil_right1:Class;
		
		//hand_skeleton_left
		[Embed(source = "/../assets/Boss/Skeleton/hand_skeleton_left/texture.png", mimeType = "image/png")]
		public static const textureImghand_devil_left1:Class;
		
		[Embed(source = "/../assets/Boss/Skeleton/hand_skeleton_left/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLhand_devil_left1:Class;
		
		[Embed(source = "/../assets/Boss/Skeleton/hand_skeleton_left/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLhand_devil_left1:Class;
		
		//BANDIT MONSTER
		
		//torso_skeleton
		[Embed(source = "/../assets/Boss/Bandit/torso_bandit/texture.png", mimeType = "image/png")]
		public static const textureImgtorso_devil2:Class;
		
		[Embed(source = "/../assets/Boss/Bandit/torso_bandit/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLtorso_devil2:Class;
		
		[Embed(source = "/../assets/Boss/Bandit/torso_bandit/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLtorso_devil2:Class;
		
		//legs_skeleton
		[Embed(source = "/../assets/Boss/Bandit/legs_bandit/texture.png", mimeType = "image/png")]
		public static const textureImglegs_devil2:Class;
		
		[Embed(source = "/../assets/Boss/Bandit/legs_bandit/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLlegs_devil2:Class;
		
		[Embed(source = "/../assets/Boss/Bandit/legs_bandit/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLlegs_devil2:Class;
		
		//head_skeleton
		[Embed(source = "/../assets/Boss/Bandit/head_bandit/texture.png", mimeType = "image/png")]
		public static const textureImghead_devil2:Class;
		
		[Embed(source = "/../assets/Boss/Bandit/head_bandit/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLhead_devil2:Class;
		
		[Embed(source = "/../assets/Boss/Bandit/head_bandit/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLhead_devil2:Class;
		
		//hand_skeleton_right
		[Embed(source = "/../assets/Boss/Bandit/hand_bandit_right/texture.png", mimeType = "image/png")]
		public static const textureImghand_devil_right2:Class;
		
		[Embed(source = "/../assets/Boss/Bandit/hand_bandit_right/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLhand_devil_right2:Class;
		
		[Embed(source = "/../assets/Boss/Bandit/hand_bandit_right/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLhand_devil_right2:Class;
		
		//hand_skeleton_left
		[Embed(source = "/../assets/Boss/Bandit/hand_bandit_left/texture.png", mimeType = "image/png")]
		public static const textureImghand_devil_left2:Class;
		
		[Embed(source = "/../assets/Boss/Bandit/hand_bandit_left/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLhand_devil_left2:Class;
		
		[Embed(source = "/../assets/Boss/Bandit/hand_bandit_left/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLhand_devil_left2:Class;
		
		
		//ROBOT MONSTER
		
		//torso_skeleton
		[Embed(source = "/../assets/Boss/Robocop/torso_robocop/texture.png", mimeType = "image/png")]
		public static const textureImgtorso_devil3:Class;
		
		[Embed(source = "/../assets/Boss/Robocop/torso_robocop/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLtorso_devil3:Class;
		
		[Embed(source = "/../assets/Boss/Robocop/torso_robocop/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLtorso_devil3:Class;
		
		//legs_skeleton
		[Embed(source = "/../assets/Boss/Robocop/legs_robocop/texture.png", mimeType = "image/png")]
		public static const textureImglegs_devil3:Class;
		
		[Embed(source = "/../assets/Boss/Robocop/legs_robocop/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLlegs_devil3:Class;
		
		[Embed(source = "/../assets/Boss/Robocop/legs_robocop/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLlegs_devil3:Class;
		
		//head_skeleton
		[Embed(source = "/../assets/Boss/Robocop/head_robocop/texture.png", mimeType = "image/png")]
		public static const textureImghead_devil3:Class;
		
		[Embed(source = "/../assets/Boss/Robocop/head_robocop/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLhead_devil3:Class;
		
		[Embed(source = "/../assets/Boss/Robocop/head_robocop/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLhead_devil3:Class;
		
		//hand_skeleton_right
		[Embed(source = "/../assets/Boss/Robocop/hand_robocop_right/texture.png", mimeType = "image/png")]
		public static const textureImghand_devil_right3:Class;
		
		[Embed(source = "/../assets/Boss/Robocop/hand_robocop_right/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLhand_devil_right3:Class;
		
		[Embed(source = "/../assets/Boss/Robocop/hand_robocop_right/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLhand_devil_right3:Class;
		
		//hand_skeleton_left
		[Embed(source = "/../assets/Boss/Robocop/hand_robocop_left/texture.png", mimeType = "image/png")]
		public static const textureImghand_devil_left3:Class;
		
		[Embed(source = "/../assets/Boss/Robocop/hand_robocop_left/texture.xml", mimeType="application/octet-stream")]
		public static const textureXMLhand_devil_left3:Class;
		
		[Embed(source = "/../assets/Boss/Robocop/hand_robocop_left/skeleton.xml", mimeType="application/octet-stream")]
		public static const _skeletonXMLhand_devil_left3:Class;
		
		private var armature:Armature;
		private var armatureClip:Sprite;
		
		private var num:int;
		
		private var onComplete:Function;
		
		private var type:String;
		
		public function DragonBoneTest(_type:String, _id:int, _onComplete:Function = null) 
		{
			type = _type;
			
			num = _id;
			onComplete = _onComplete;
			
			touchable = false;
			
			create();
		}
		
		public function create():void
		{
			//var nameVector:Vector.<String> = new <String>["hand_devil_right", "yellow", "orange", "red", "pink", "blue", "grey"];
			
			addChild(make(type + num, type/*nameVector[num]*/, type/*nameVector[num]/* + "_db"*/));
		}
		
		private function make(id:String, simpleName:String, textureName:String):Sprite 
		{
			if (!skeletonArr[id])
			{
				var skeletonXML:XML = new XML(new DragonBoneTest["_skeletonXML" + id]());
				skeletonArr[id] = XMLDataParser.parseSkeletonData(skeletonXML);
			}
			
			if (!textureArr[id])
			{
				var texture:Texture = Texture.fromBitmap(new DragonBoneTest["textureImg" + id]());
				textureArr[id] = new StarlingTextureAtlas(texture, new XML(new DragonBoneTest["textureXML" + id]()), true);
			}
			
			if (!factoryArr[id])
			{
				factoryArr[id] = new StarlingFactory();
				factoryArr[id].addSkeletonData(skeletonArr[id], simpleName);
				factoryArr[id].addTextureAtlas(textureArr[id], textureName);
			}
			
			armature = factoryArr[id].buildArmature(textureName, null, simpleName, textureName);
			
			//armature.addEventListener(AnimationEvent.COMPLETE, onAnimationComplete);
			armature.animation.gotoAndPlay(stateIdle);
			
			WorldClock.clock.add(armature);
			armature.display.touchable = false;
			return armature.display as Sprite;//addChild(armature.display as Sprite);
		}
		
		public function play(state:String):void
		{
			if (armature)
				armature.animation.gotoAndPlay(state);
		}
		
		/**
		 * Ovveriding hitTest, so this class and all children do not trigger any Touches
		 * @param	localPoint
		 * @param	forTouch
		 * @return
		 */
		public override function hitTest(localPoint:Point, forTouch:Boolean = false):DisplayObject
		{
			return null;// super.hitTest(localPoint, forTouch) ? this : null;
		}
		
		private function onAnimationComplete(e:AnimationEvent):void 
		{
			armature.removeEventListener(AnimationEvent.COMPLETE, onAnimationComplete);
			
			if (onComplete != null)
			{
				onComplete();
				return;
			}
			
			armature.animation.gotoAndPlay(stateIdle);
		}
		
		public function pause():void
		{
			WorldClock.clock.remove(armature);
		}
		
		public function resume():void
		{
			WorldClock.clock.add(armature);
		}
		
		public function remove():void
		{
			if (armature)
				armature.removeEventListener(AnimationEvent.COMPLETE, onAnimationComplete);
			
			onComplete = null;
			WorldClock.clock.remove(armature);
			removeChild(armature.display as Sprite);
		}
		
	}

}