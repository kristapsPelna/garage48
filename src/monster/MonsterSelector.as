package monster {
	import mechanics.MonsterBoxLibrary;
	import monster.behaviours.BasicFollower;
	import monster.behaviours.Patrol;
	import monster.behaviours.PatrolChaotic;
	import monster.behaviours.Scared;
	import monster.behaviours.Standing;
	import monster.parts.Body;
	import monster.parts.Head;
	import monster.parts.LeftArm;
	import monster.parts.Legs;
	import monster.parts.RightArm;
	import objects.Box;
	
	/**
	 * Monstra veidotājs
	 * @author Kamazs
	 */
	public class MonsterSelector {
		
		public static const behaviourPool:Array = [BasicFollower, /*Scared,*/ Standing, Patrol, PatrolChaotic];
		
		public static function getBehaviour():AI {
			var classRef:Class = MonsterSelector.behaviourPool[Constants.random.nextMinMax(0, behaviourPool.length)];
			return new classRef() as AI;
		}
		
		public static function selectBoss(boss:Boss):void {
			var parts:Vector.<Box> = new Vector.<Box>(Boss.PART_COUNT);
			
			parts[Boss.IDX_HEAD] = MonsterBoxLibrary.getRandom(DragonBoneTest.head, Head);
			parts[Boss.IDX_BODY] = MonsterBoxLibrary.getRandom(DragonBoneTest.torso, Body);
			parts[Boss.IDX_LEFT_ARM] = MonsterBoxLibrary.getRandom(DragonBoneTest.handLeft, LeftArm);
			parts[Boss.IDX_RIGHT_ARM] = MonsterBoxLibrary.getRandom(DragonBoneTest.handRight, RightArm);
			parts[Boss.IDX_LEGS] = MonsterBoxLibrary.getRandom(DragonBoneTest.legs, Legs);
			
			boss.setParts(parts);
		}
	}

}