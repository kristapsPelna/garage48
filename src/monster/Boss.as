package monster
{
	import flash.geom.Point;
	import flash.utils.getTimer;
	import gui.GoodJobWindow;
	import mechanics.MonsterBoxLibrary;
	import monster.parts.Body;
	import monster.parts.Head;
	import monster.parts.LeftArm;
	import monster.parts.Legs;
	import monster.parts.Part;
	import monster.parts.RightArm;
	import objects.Box;
	import objects.Bullet;
	import objects.Character;
	import objects.Room;
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	import util.MonsterSortMC;
	import util.Notification;
	import util.NotificationCenter;
	
	/**
	 * BIG BAD BOSS!
	 * @author Kamazs
	 */
	public class Boss extends Box
	{
		public static const PART_COUNT:int = 5;
		
		public static const IDX_HEAD:int = 0;
		public static const IDX_BODY:int = 1;
		public static const IDX_LEFT_ARM:int = 2;
		public static const IDX_RIGHT_ARM:int = 3;
		public static const IDX_LEGS:int = 4;
		
		public var head:Head;
		public var body:Body;
		public var leftArm:LeftArm;
		public var rightArm:RightArm;
		public var legs:Legs;
		
		public var sizeType:int;
		public var moodType:int;
		public var toughnessType:int;
		public var scale:Number;
		public var difficultyRating:Number;
		
		private var plane:Sprite;
		private var lastUpdate:int;
		
		public var parts:Vector.<Box>;
		public var ai:AI;
		
		private var behaviorChangeTimeMin:int = 10;
		private var behaviorChangeTimeMax:int = 180;
		
		
		private static var _instance:Boss;
		private var sprBase:Sprite;
		
		public static function get instance():Boss	{
			return _instance;
		}
		
		public function Boss() {
			
			changeAI();
			_instance = this;
			
			super();
		}
		
		private function changeAI():void 
		{
			this.ai = MonsterSelector.getBehaviour();
			
			trace("Boss mood: ", ai.name);
			ai.boss = this;
			
			if (ai.name == "standing")
				var changeAfter:int = Constants.random.nextMinMax(5, 15);
			else
				changeAfter = Constants.random.nextMinMax(behaviorChangeTimeMin, behaviorChangeTimeMax);
			trace("Boss mood change after", changeAfter);
			
			Starling.juggler.delayCall(changeAI, changeAfter);
		}
		
		override public function create():void
		{
			super.create();
			
			// setup
			
			maxSpeed = 2;
			speed.setTo(maxSpeed, maxSpeed);
			walkThrough  = true;
			lastUpdate = getTimer();
			ai.onCreated();
			
			group = "boss";
			
		}
		
		public function activate():void {
			Starling.current.stage.addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
		}
		
		public function setParts(partVector:Vector.<Box>):void {
			
			if (parts != null) {
				for (var p:int = 0; p < PART_COUNT; p++) {
					if (parts[p]!=null && parts[p].parent !=null){
						parts[p].removeFromParent();
					}
				}				
			}
			
			parts = partVector;
			
			head = parts[IDX_HEAD] as Head;
			body = parts[IDX_BODY] as Body;
			leftArm = parts[IDX_LEFT_ARM] as LeftArm;
			rightArm = parts[IDX_RIGHT_ARM] as RightArm;
			legs = parts[IDX_LEGS] as Legs;
			
			maxHP = 0;
			maxStamina = 0;
			hpRegen = 0;
			staminaRegen = 0;
			bossScale = 0.0;
			
			for (var i:int = 0; i < PART_COUNT; i++) {
				maxHP += parts[i].maxHP;
				maxStamina += parts[i].maxStamina;
				hpRegen += parts[i].hpRegen;
				staminaRegen += parts[i].staminaRegen;
				parts[i].referenceBox = this;
				bossScale += parts[i].bossScale;
			}
			maxSpeed = legs.maxSpeed;
			hp =  maxHP;
			stamina = maxStamina;
			dmg = 10;
			setScale(bossScale);
			legs.animation.play(DragonBoneTest.stateIdle);
			
			trace("BOSS's hp ",hp," no maxHP: ", maxHP);
			trace("maxStamina: ", maxStamina);
			trace("hpRegen: ", hpRegen);
			trace("staminaRegen:", staminaRegen);
			trace("maxSpeed:", maxSpeed);
			
			
			positionParts();
			
			reset();
			//for (var i:int = 0; i<parts.le
		}
		
		private function onEnterFrame(e:EnterFrameEvent):void 
		{
			var char:Character = Character.instance;
			var location:Point = new Point(char.x + char.width * 0.5, Character.instance.y + char.height * 0.5);
			
			if (head && head.weapon)
				head.shoot(location);
			
			if (leftArm && leftArm.weapon)
				leftArm.shoot(location);
			
			if (rightArm && rightArm.weapon)
				rightArm.shoot(location);
		}
		
		override public function destroy():void 
		{
			for each (var part:Part in parts) {
				part.destroy();
				part = null;
			}
			parts = null;
			
			head = null;
			body = null;
			rightArm = null;
			leftArm = null;
			legs = null;
			
			_instance = null;
			
			super.destroy();
			
			Starling.current.stage.removeEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
			
			Game.instance.endGame(true);
		}
		
		override public function reset():void
		{
			//positionParts();
			super.reset();
		}
		
		private function positionParts():void
		{
			if (sprBase != null) {
				sprBase.removeChildren();
			}
			if (sprBase && sprBase.parent != null) {
				sprBase.removeFromParent();
			}
			sprBase = new Sprite();
			
			legs.x = legs.width / 2;
			legs.y = legs.height;
			sprBase.addChild(legs);
			
			body.x = legs.x; // + (legs.width - body.width) / 2;
			body.y = legs.bounds.top; // - body.height + 5;
			sprBase.addChild(body);
			
			head.x = body.x; // + (body.width - head.width) / 2;
			head.y = body.bounds.top + 35;
			sprBase.addChild(head);
			
			leftArm.x = body.bounds.left + 35; // rightArm.width / 2;
			leftArm.y = body.bounds.top + 45;
			sprBase.addChildAt(leftArm, 1);
			
			rightArm.x = body.bounds.right - 35;
			rightArm.y = body.bounds.top + 45;
			sprBase.addChildAt(rightArm, 1);
			
			sprBase.alignPivot(HAlign.LEFT, VAlign.TOP);
			addChild(sprBase);
		}
		
		override public function onCollide(box:Box):void
		{
			super.onCollide(box);
			if (box is Bullet)
			{
				if (!Starling.juggler.containsTweens(this))
				{
					blink();
					box.destroy();
						//addChild(new Fades(0, -160, "-10", { color: Color.RED, totalTime: 3.0, floatY: 80, size: 25} ));
				}
			}
			
			ai.onCollision(box);
		}
		
		public function blink():void
		{
			Starling.juggler.tween(this, 0.15, {transition: Transitions.EASE_OUT, alpha: 0.75, reverse: true, repeatCount: 2});
		}
		
		override public function onAttack(victim:Box):void 		{
			super.onAttack(victim);
			
			head.setMode(DragonBoneTest.stateAttack);
			if (victim.x < x) {
				leftArm.setMode(DragonBoneTest.stateAttack);
			} else {
				rightArm.setMode(DragonBoneTest.stateAttack);
			}
			
			ai.onAttack(victim);
		}
		
		override public function onUpdate():void 		{
			super.onUpdate();
			
			if (ai.onUpdate(getTimer() - lastUpdate)) {
				lastUpdate = getTimer();
			}
			MonsterSortMC.swapMC(this);
		}
		
		public function setScale(scale:Number):void
		{
			this.scale = scale;
			scaleX = scaleY = scale;
		}
		
		override public function setHP(hp:Number):void 		{
			super.setHP(hp);
			NotificationCenter.post(Notification.monsterHpChange);
		}
	
	}

}