package monster 
{
	import starling.display.Image;
	import util.Media;
	/**
	 * ...
	 * @author Kamazs
	 */
	public class TmpAnimation extends Animation {
		
		public function TmpAnimation(img:String) 	{
			super("", -1);
			
			var image:Image = new Image( Media.instance.getTexture(img) );
			addChild(image);
		}
		
	}

}