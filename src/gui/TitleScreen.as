package gui 
{
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import util.Media;
	/**
	 * ...
	 * @author Kamazs
	 */
	public class TitleScreen extends Container 
	{
		private var callback:Function;
		public function TitleScreen(closeCallback:Function) 
		{
			super();
			this.callback = closeCallback;
			open();
		}
		
		override public function build():void 
		{
			super.build();
			
			addChild( new Image(Media.instance.getTexture("title") ) );
			
			var btnContinue:Button = new Button( Media.instance.getTexture("play01") );
			btnContinue.x = this.width - 360 + btnContinue.width/2;
			btnContinue.y = this.height / 2 - 100  +btnContinue.height/2;
			addChild(btnContinue);
			btnContinue.alignPivot();
			
			btnContinue.addEventListener(TouchEvent.TOUCH, onTouchBtn);
		}
		
		private function onTouchBtn(e:TouchEvent):void 		{
			var btn:Button = e.currentTarget as Button;
			if (btn == null) {
				return;
			}
			
			var touch:Touch = e.getTouch(btn);
			if (!touch) {
				Starling.juggler.removeTweens(btn);
				Starling.juggler.tween(btn, 0.55, {
						scaleX: 1.0,
						scaleY: 1.0,
						transition: Transitions.EASE_OUT
					}
				);			
				return;
			}
			
			
			
			switch (touch.phase) {
				case TouchPhase.ENDED:
					btn.removeEventListeners();
					close();
					break;
				case TouchPhase.HOVER:
					if (!Starling.juggler.containsTweens(btn)) {
						Starling.juggler.tween(btn, 0.55, {
							scaleX: 1.1,
							scaleY: 1.1,
							transition: Transitions.EASE_OUT
						});
					}
					break;
					
			}
		}
		
		override public function close():void 
		{
			super.close();
			if (callback != null) {
				callback();
			}
		}
		
	}

}