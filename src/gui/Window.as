package gui 
{
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import util.Media;
	/**
	 * Izlecošajiem logiem vienojoša klase
	 * @author Kamazs
	 */
	public class Window extends Container 	{
		private var callback:Function;
		public function Window(callback:Function) 		{
			super();
			this.callback = callback;
			open();
		}
		
		override public function build():void 		{
			super.build();
			
			addChild( new Image(Media.instance.getTexture("logs") ) );
		}
		
		public function center():void {
			alignPivot();
			x = Constants.stageW/ 2;
			y = Constants.stageH / 2;
		}
		
		protected function onTouchBtn(e:TouchEvent):void 		{
			var btn:Button = e.currentTarget as Button;
			if (btn == null) {
				return;
			}
			
			var touch:Touch = e.getTouch(btn);
			if (!touch) {
				Starling.juggler.removeTweens(btn);
				Starling.juggler.tween(btn, 0.55, {
						scaleX: 1.0,
						scaleY: 1.0,
						transition: Transitions.EASE_OUT
					}
				);			
				return;
			}
			
			switch (touch.phase) {
				case TouchPhase.ENDED:
					btn.removeEventListeners();
					close();
					break;
				case TouchPhase.HOVER:
					if (!Starling.juggler.containsTweens(btn)) {
						Starling.juggler.tween(btn, 0.55, {
							scaleX: 1.1,
							scaleY: 1.1,
							transition: Transitions.EASE_OUT
						});
					}
					break;
					
			}
		}
		
		override public function open():void 
		{
			center();
			super.open();
		}
		
		override public function appear():void 
		{
			scaleX = scaleY = 0.0;
			Starling.juggler.removeTweens(this);
			Starling.juggler.tween(this, 0.75, {
				scaleX: 1.0,
				scaleY: 1.0,
				transition: Transitions.EASE_OUT_BOUNCE,
				onComplete: appeared
			});
		}
		
		override public function close():void 
		{
			super.close();
			if (callback != null) {
				callback();
			}
		}	
		
		override public function disappear():void 
		{
			scaleX = scaleY = 1.0;
			Starling.juggler.removeTweens(this);
			Starling.juggler.tween(this, 0.75, {
				scaleX: 0.0,
				scaleY: 0.0,
				transition: Transitions.EASE_OUT_BOUNCE,
				onComplete: disappeared
			});
		}
		
	}

}