package gui 
{
	import feathers.controls.ProgressBar;
	import feathers.themes.BaseMetalWorksMobileTheme;
	import feathers.themes.MetalWorksMobileTheme;
	import monster.Boss;
	import objects.Character;
	import starling.display.Sprite;
	import starling.text.TextField;
	import util.Notification;
	import util.NotificationCenter;
	/**
	 * ...
	 * @author Ace
	 */
	public class GUI extends Sprite
	{
		private var healthBar:ProgressBar;
		private var healthText:TextField;
		private var monsterBarHP:ProgressBar;
		
		public function GUI() 
		{
			new MetalWorksMobileTheme();
			
			healthBar = new ProgressBar();
			healthBar.x = healthBar.y = 10;
			healthBar.minimum = 0;
			healthBar.maximum = Character.instance.maxHP;
			
			healthBar.width = 200;
			healthBar.height = 40;
			
			healthText = new TextField(healthBar.width, healthBar.height, "", "Verdana", 22, 0xFFFFFF, true);
			healthText.x = healthText.y = healthBar.x;
			
			onHealthChange();
			
			addChild(healthBar);
			addChild(healthText);
			
			NotificationCenter.add(this, Notification.healthChange, onHealthChange);
			
			if (Boss.instance!=null){
			
				// monster health
				monsterBarHP = new ProgressBar();
				monsterBarHP.minimum = 0;
				monsterBarHP.maximum = Boss.instance.maxHP;
				monsterBarHP.width = 200;
				monsterBarHP.height = 40;	
				monsterBarHP.x = Constants.stageW - monsterBarHP.width - 10;
				monsterBarHP.y = healthBar.y;			
				
				addChild(monsterBarHP);
				
				
				NotificationCenter.add(this, Notification.monsterHpChange, onMonsterHPChange);
			}
		}
		
		private function onHealthChange():void 
		{
			healthBar.value = Character.instance.hp;
			healthText.text = "HP: " + Character.instance.hp + "/" + Character.instance.maxHP;
		}
		
		private function onMonsterHPChange():void {
			monsterBarHP.value = Boss.instance.hp;
		}
		
	}

}