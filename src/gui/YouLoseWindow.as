package gui 
{
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.TouchEvent;
	import util.Media;
	/**
	 * ...
	 * @author Kamazs
	 */
	public class YouLoseWindow extends Window 
	{
		
		public function YouLoseWindow(callback:Function) 		{
			super(callback);
		}
		
		override public function build():void 		{
			super.build();
			
			var infoGfx:Image = new Image( Media.instance.getTexture("you-died") );
			infoGfx.alignPivot();
			infoGfx.x = width / 2;
			infoGfx.y = height / 2 - 30;
			addChild(infoGfx);
			
			var btnOK:Button = new Button( Media.instance.getTexture("ok") );
			btnOK.alignPivot();
			btnOK.x = width / 2;
			btnOK.y = height / 2 + 20;
			addChild(btnOK);
			
			btnOK.addEventListener(TouchEvent.TOUCH, onTouchBtn);
		}
		
	}

}