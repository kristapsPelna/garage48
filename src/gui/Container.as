package gui 
{
	import flash.geom.Point;
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Sprite;
	
	/**
	 * Konteinera klase, pamats logiem un popupiem
	 * @author Kamazs
	 */
	public class Container extends Sprite 	{
		
		public function Container() 		{
			super();
			build();
		}
		
		public function build():void {
			
		}
		
		public function open():void {
			appear();
		}
		
		public function appear():void {
			var targetX:Number = x;
			x = -this.width - 5;
			Starling.juggler.removeTweens(this);
			Starling.juggler.tween(this, 0.75, {
				x: targetX,
				transition: Transitions.EASE_OUT_BACK,
				onComplete: appeared
			});
		}
		
		public function appeared():void {
			
		}
		
		public function close():void {
			disappear();
		}
		
		public function disappear():void {
			Starling.juggler.removeTweens(this);
			Starling.juggler.tween(this, 0.75, {
				x: -this.width - 5,
				transition: Transitions.EASE_IN,
				onComplete: disappeared
			});			
		}
		
		public function disappeared():void {
			this.removeFromParent(true);
		}
		
	}

}