package mechanics {
	import flash.geom.Point;
	import flash.ui.Keyboard;
	import monster.Boss;
	import monster.MonsterSelector;
	import objects.Box;
	import objects.Character;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.events.EnterFrameEvent;
	import starling.events.KeyboardEvent;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import util.Debugger;
	/**
	 * ...
	 * @author Ace
	 */
	public class Controller 
	{
		public var object:Box;
		
		private const UP:String = "up";
		private const DOWN:String = "down";
		private const LEFT:String = "left";
		private const RIGHT:String = "right";
		
		private var holdingDown:Boolean = false;
		private var holdingUp:Boolean = false;
		private var holdingLeft:Boolean = false;
		private var holdingRight:Boolean = false;
		
		public static var instance:Controller;
		
		public function Controller(_object:Box) 
		{
			object = _object;
			
			if (!object)
				return;
				
			instance = this;
			
			Starling.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, onStageKey);
			Starling.current.stage.addEventListener(KeyboardEvent.KEY_UP, onStageKeyRelease);
			
			Starling.current.stage.addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
			
			Starling.current.stage.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private function onTouch(e:TouchEvent):void 
		{
			var touch:Touch = e.getTouch(Starling.current.stage, TouchPhase.BEGAN);
			var touchUp:Touch = e.getTouch(Starling.current.stage, TouchPhase.ENDED);
			
			if (touch && object is Character)
			{
				var location:Point = touch.getLocation(Starling.current.stage);
				Character(object).shoot(location);
			}
		}
		
		private function onEnterFrame(e:EnterFrameEvent):void 
		{
			move();
		}
		
		private function onStageKeyRelease(e:KeyboardEvent):void 
		{
			switch (e.keyCode)
			{
				case Keyboard.UP:
				case 87:
					holdingUp = false;
					break;
				case Keyboard.DOWN:
				case 83:
					holdingDown = false;
					break;
				case Keyboard.LEFT:
				case 65:
					holdingLeft = false;
					break;
				case Keyboard.RIGHT:
				case 68:
					holdingRight = false;
					break;
				case Keyboard.NUMPAD_SUBTRACT:
					
					Game.instance.room.reset();
					break;
			}
			
			Character.instance.stopMovement();
		}
		
		private function onStageKey(e:KeyboardEvent):void 
		{
			switch (e.keyCode)
			{
				case Keyboard.UP:
				case 87:
					holdingUp = true;
					break;
				case Keyboard.DOWN:
				case 83:
					holdingDown = true;
					break;
				case Keyboard.LEFT:
				case 65:
					holdingLeft = true;
					break;
				case Keyboard.RIGHT:
				case 68:
					holdingRight = true;
					break;
			}
		}
		
		private function move():void 
		{
			if (!object) {
				return;
			}
			
			var direction:Point = new Point(0, 0);

			if (holdingUp)
				direction.y -= 1;//move(UP);
			if (holdingDown)
				direction.y += 1;//move(DOWN);
			if (holdingLeft)
				direction.x -= 1;//move(LEFT);
			if (holdingRight)
				direction.x += 1;//move(RIGHT);
			
			if (direction.x != 0 || direction.y != 0)
				object.moveInDirection(direction.x, direction.y);
		}
		
		public function destroy():void {
			Starling.current.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onStageKey);
			Starling.current.stage.removeEventListener(KeyboardEvent.KEY_UP, onStageKeyRelease);
			
			Starling.current.stage.removeEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
			
			Starling.current.stage.removeEventListener(TouchEvent.TOUCH, onTouch);
		}
		
	}

}