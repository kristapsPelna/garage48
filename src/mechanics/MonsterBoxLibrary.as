package mechanics 
{
	import monster.Animation;
	import monster.Boss;
	import monster.DragonBoneTest;
	import monster.parts.Legs;
	import monster.parts.Part;
	import objects.Box;
	/**
	 * ...
	 * @author Kamazs
	 */
	public class  MonsterBoxLibrary 	{
		
		public function MonsterBoxLibrary() {
		
		}
		
		public static const parts:Object = 
			{
				"legs_devil":[
						{
							race: "criminal",
							type: DragonBoneTest.legs,
							maxSpeed:3,
							acceleration:1,
							solid: true,
							animationId:2,
							maxHP:180,
							hp:180,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.12,
							dmg: 10
						},
						{
							race: "demon",
							type: DragonBoneTest.legs,
							maxSpeed:1.5,
							acceleration:1,
							solid: true,
							animationId:0,
							maxHP:240,
							hp:240,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.2,
							dmg: 10
						},
						{
							race: "undead",
							type: DragonBoneTest.legs,
							maxSpeed:2.5,
							acceleration:1,
							solid: true,
							animationId:1,
							maxHP:120,
							hp:120,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.16,
							dmg: 10
						},
						{
							race: "robot",
							type: DragonBoneTest.legs,
							maxSpeed:2,
							acceleration:1,
							solid: true,
							animationId:3,
							maxHP:300,
							hp:300,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.24,
							dmg: 10
						}
					],
					"head_devil":[
						{
							race: "criminal",
							type: DragonBoneTest.head,
							maxSpeed:0,
							acceleration:0,
							solid: true,
							animationId:2,
							maxHP:300,
							hp:300,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.12,
							weapon: null,
							dmg: 10
						},
						{
							race: "demon",
							type: DragonBoneTest.head,
							maxSpeed:0,
							acceleration:1,
							solid: true,
							animationId:0,
							maxHP:400,
							hp:400,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							weapon: null,
							scale: 0.2,
							dmg: 10
						},
						{
							race: "undead",
							type: DragonBoneTest.head,
							maxSpeed:0,
							acceleration:1,
							solid: true,
							animationId:1,
							maxHP:200,
							hp:200,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.16,
							weapon: null,
							dmg: 10
						},
						{
							race: "robot",
							type: DragonBoneTest.head,
							maxSpeed:0,
							acceleration:1,
							solid: true,
							animationId:3,
							maxHP:500,
							hp:500,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							weapon: 6,
							scale: 0.24,
							dmg: 10
						}
					],
					"torso_devil":[
						{
							race: "criminal",
							type: DragonBoneTest.torso,
							maxSpeed:0,
							acceleration:0,
							solid: true,
							animationId:2,
							maxHP:600,
							hp:600,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.12,
							dmg: 10
						},
						{
							race: "demon",
							type: DragonBoneTest.torso,
							maxSpeed:0,
							acceleration:1,
							solid: true,
							animationId:0,
							maxHP:8500,
							hp:800,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.2,
							dmg: 10
						},
						{
							race: "undead",
							type: DragonBoneTest.torso,
							maxSpeed:0,
							acceleration:1,
							solid: true,
							animationId:1,
							maxHP:400,
							hp:400,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.16,
							dmg: 10
						},
						{
							race: "robot",
							type: DragonBoneTest.torso,
							maxSpeed:0,
							acceleration:1,
							solid: true,
							animationId:3,
							maxHP:1000,
							hp:1000,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.24,
							dmg: 10
						}
					],
					"hand_devil_right":[
						{
							race: "criminal",
							type: DragonBoneTest.handRight,
							maxSpeed:0,
							acceleration:0,
							solid: true,
							animationId:2,
							maxHP:180,
							hp:180,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							weapon: 2,
							scale: 0.12,
							dmg: 10
						},
						{
							race: "demon",
							type: DragonBoneTest.handRight,
							maxSpeed:0,
							acceleration:1,
							solid: true,
							animationId:0,
							maxHP:240,
							hp:240,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.2,
							weapon: 4,
							dmg: 10
						},
						{
							race: "undead",
							type: DragonBoneTest.handRight,
							maxSpeed:0,
							acceleration:1,
							solid: true,
							animationId:1,
							maxHP:120,
							hp:120,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.16,
							weapon: 7,
							dmg: 10
						},
						{
							race: "robot",
							type: DragonBoneTest.handRight,
							maxSpeed:0,
							acceleration:1,
							solid: true,
							animationId:3,
							maxHP:300,
							hp:300,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.24,
							weapon: 14,
							dmg: 10
						}	
					],
					"hand_devil_left": [
						{
							race: "criminal",
							type: DragonBoneTest.handLeft,
							maxSpeed:0,
							acceleration:0,
							solid: true,
							animationId:2,
							maxHP:180,
							hp:180,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.12,
							weapon: 3,
							dmg: 10
						},
						{
							race: "demon",
							type: DragonBoneTest.handLeft,
							maxSpeed:0,
							acceleration:1,
							solid: true,
							animationId:0,
							maxHP:240,
							hp:240,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.2,
							weapon: 5,
							dmg: 10
						},
						{
							race: "undead",
							type: DragonBoneTest.handLeft,
							maxSpeed:0,
							acceleration:1,
							solid: true,
							animationId:1,
							maxHP:120,
							hp:120,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.16,
							weapon: 8,
							dmg: 10
						},
						{
							race: "robot",
							type: DragonBoneTest.handLeft,
							maxSpeed:0,
							acceleration:1,
							solid: true,
							animationId:3,
							maxHP:300,
							hp:300,
							hpRegen: 0,
							attackType: null,
							effects:  null,
							scale: 0.24,
							weapon: 15,
							dmg: 10
						}
					]
			};
		
		public static function createPart(params:Object, classReference:Class):Part {
			var monsterBox:Part = new classReference();
			monsterBox.type = params.type;
			monsterBox.maxSpeed = params.maxSpeed;
			monsterBox.solid = params.solid;
			var idx:int = params.animationId;
			if (idx > DragonBoneTest.totalVariations-1) {
				idx = DragonBoneTest.totalVariations-1;
			}
			var animation:Animation = new Animation(params.type, idx);
			monsterBox.setAnimation(animation);
			monsterBox.maxHP = params.maxHP;
			monsterBox.setHP(params.hp);
			monsterBox.hpRegen = params.hpRegen;
			//monsterBox.attackType = params.attackType as Vector.<Effect>;
			//monsterBox.effects = params.effects as Vector.<Effect>;
			monsterBox.maxStamina = params.maxStamina;
			monsterBox.setStamina(params.stamina);
			monsterBox.staminaRegen = params.staminaRegen;
			//monsterPart.resistances = [];
			monsterBox.timeToLive = params.timeToLive;
			monsterBox.race = params.race;
			if (params.weapon){
				monsterBox.weapon = Arsenal.getWeapon(params.weapon);
				monsterBox.weapon.activateCooldown();
				monsterBox.weapon.owner = monsterBox;
			}
			monsterBox.bossScale = params.scale;
			
			return monsterBox;
		}
		
		public static function getRandom(type:String, classReference:Class):Part {
			var arr:Array = parts[type];
			var rndIdx:int = Constants.random.nextMinMax(0, arr.length);
			var params:Object = arr[rndIdx];
			
			var part:Part = createPart(params, classReference);
			return part;
		}
		
		
	}

}