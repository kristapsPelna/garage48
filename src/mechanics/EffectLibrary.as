package mechanics 
{
	/**
	 * ...
	 * @author Ace
	 */
	public class EffectLibrary 
	{
		//       													 minDMG maxDMG stepCount stepInterval duration moveSpeedKoef attackSpeedKoef regenHPKoef staminaRegenKoef damageOverTime staminaCosthpCost hpGain staminaGain)
		public static const poison:Effect = new Effect("poison",       10,	  15,		0,			0,       0,         0,            0,             0,           0,             0,                0,            0,     0);
		public static const explosion:Effect = new Effect("explosion", 10,    15);
		
		public static const gooPuddle:Effect = new Effect("gooPuddle");
		public static const wormSpawner:Effect = new Effect("wormSpawner");
		public static const weakness:Effect = new Effect("weakness");
		public static const flySpawner:Effect = new Effect("flySpawner");
		public static const mineSpawner:Effect = new Effect("mineSpawner");
		public static const scream:Effect = new Effect("scream");
		
		public function EffectLibrary() 
		{
			
		}
		
	}

}