package mechanics {
	import flash.geom.Point;
	import monster.Boss;
	import monster.parts.Part;
	import objects.Box;
	import objects.Bullet;
	import objects.Character;
	import objects.Room;
	import starling.core.Starling;
	import util.Fades;
	import util.UtilLib;
	/**
	 * ...
	 * @author Ace
	 */
	public class Weapon 
	{
		public var name:String;
		
		public var speedMin:Number;
		public var speedMax:Number;
		
		public var dmgMin:Number;
		public var dmgMax:Number;
		
		public var spreadAngle:Number;
		private var currentSpreadAngle:Number;
		
		public var effects:Array;
		
		public var repeatedShots:int;
		public var shotDelay:Number;
		public var cooldown:Number;
		
		public var bulletsPerShot:int;
		
		public var timeToLive:Number;
		
		public var bulletType:int;
		
		public var owner:Box;
		
		private var jammed:Boolean = false;
		
		public function Weapon(_name:String, _dmgMin:Number = 0, _dmgMax:Number = 0, _speedMin:Number = 0, _speedMax:Number = 0, _spreadAngle:Number = 0, _repeatedShots:int = 0, _shotDelay:Number = 0, _cooldown:Number = 0, _bulletsPerShot:int = 0, _timeToLive:Number = 0, _bulletType:int = 0, _effects:Array = null) 
		{
			name = _name;
			
			dmgMin = _dmgMin;
			dmgMax = _dmgMax;
			
			speedMin = _speedMin;
			speedMax = _speedMax;
			spreadAngle = _spreadAngle;
			
			repeatedShots = _repeatedShots;
			shotDelay = _shotDelay;
			cooldown = _cooldown;
			
			bulletsPerShot = _bulletsPerShot;
			timeToLive = _timeToLive;
			
			bulletType = _bulletType;
			
			effects = _effects;
		}
		
		public function clone():Weapon
		{
			return new Weapon(name, dmgMin, dmgMax, speedMin, speedMax, spreadAngle, repeatedShots, shotDelay, cooldown, bulletsPerShot, timeToLive, bulletType, effects);
		}
		
		public function shoot(location:Point):Boolean
		{
			if (jammed)
				return false;
			
			/*bulletsPerShot = 2;
			shotDelay = 0.25;
			cooldown = 3;
			timeToLive = 0.2;*/
			
			activateCooldown();
			
			var spreadAngles:Array = [];
			
			for (var i:int = 0; i < repeatedShots; i++)
			{
				for (var j:int = 0; j < bulletsPerShot; j++)
				{
					if (spreadAngle > 0)
					{
						if (!spreadAngles[j])
							spreadAngles[j] = currentSpreadAngle = Constants.random.nextMinMax(0 - spreadAngle * 0.5, spreadAngle - spreadAngle * 0.5);
						else
							currentSpreadAngle = spreadAngles[j];
					}
					else
						currentSpreadAngle = 0;
					
					Starling.juggler.tween(this, shotDelay * i, { onComplete:createBullet, onCompleteArgs:[i, location, currentSpreadAngle] } );
				}
			}
			
			return true;
		}
		
		public function activateCooldown():void 
		{
			if (cooldown > 0)
			{
				jammed = true;
				Starling.juggler.delayCall(unjam, cooldown);
			}
		}
		
		private function unjam():void 
		{
			jammed = false;
		}
		
		private function createBullet(i:int, location:Point, currentSpreadAngle:Number):void 
		{
			if (!owner) {
				return;
			}
			
			var ownerPos:Point = new Point(owner.x, owner.y);
			
			if (owner is Part)
			{
				ownerPos = Room.instance.localToGlobal(ownerPos);
				ownerPos.x += Boss.instance.x;
				ownerPos.y += Boss.instance.y;
				
				ownerPos.x += owner.width * 0.5;
				ownerPos.y += owner.height * 0.5;
			}
			else if (owner is Character)
			{
				ownerPos.x += Character(owner).aimPoint.x;
				ownerPos.y += Character(owner).aimPoint.y;
			}
			
			var angle:Number = UtilLib.angleBetween(ownerPos.x, ownerPos.y, location.x, location.y) + 180;
			
			if (owner is Character)
			{
				var leftOver:int = angle % 90;
				
				if (leftOver < 45)
					angle -= leftOver;
				else
					angle += 90 - leftOver;
			}
			
			angle += currentSpreadAngle;
			
			//ownerPos = Starling.current.stage.localToGlobal(ownerPos);
			
			var bullet:Bullet = new Bullet(this, UtilLib.radians(angle), location);
			bullet.x = ownerPos.x;
			bullet.y = ownerPos.y;
			
			if (owner is Part)
				bullet.ignoreGroup = "boss";
			else if (owner is Character)
				bullet.ignoreGroup = "player";
			
			Room.instance.add(bullet);
			Room.instance.add(new Fades(bullet.x + bullet.width * 0.5, bullet.y + bullet.height * 0.5, "Pow!"));
			
			if (timeToLive > 0)
				Starling.juggler.delayCall(bullet.destroy, timeToLive);
			
			Collider.instance.add(bullet);
		}
		
	}

}