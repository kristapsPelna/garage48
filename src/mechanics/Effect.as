package mechanics 
{
	import objects.Box;
	import starling.core.Starling;
	/**
	 * ...
	 * @author Ace
	 */
	public class Effect 
	{
		public var name:String;
		
		public var minDMG:Number;
		public var maxDMG:Number;
		
		public var stepCount:Number;
		public var stepInterval:Number;
		public var duration:Number;
		
		public var moveSpeedKoef:Number;
		public var attackSpeedKoef:Number;
		public var regenHPKoef:Number;
		public var staminaRegenKoef:Number;
		public var damageOverTime:Number;
		
		public var staminaCost:Number;
		public var hpCost:Number;
		public var hpGain:Number;
		public var staminaGain:Number;
		
		public function Effect(_name:String, _minDMG:Number = 0, _maxDMG:Number = 0, _stepCount:Number = 0, _stepInterval:Number = 0, _duration:Number = 0, _moveSpeedKoef:Number = 0, _attackSpeedKoef:Number = 0, _regenHPKoef:Number = 0, _staminaRegenKoef:Number = 0, _damageOverTime:Number = 0, _staminaCost:Number = 0, _hpCost:Number = 0, _hpGain:Number = 0, _staminaGain:Number = 0)
		{
			name = _name;
			minDMG = _minDMG;
			maxDMG = maxDMG;
			
			stepCount = _stepCount;
			stepInterval = _stepInterval;
			duration = _duration;
			
			moveSpeedKoef = _moveSpeedKoef;
			attackSpeedKoef = _attackSpeedKoef;
			regenHPKoef = _regenHPKoef;
			staminaRegenKoef = _staminaRegenKoef;
			damageOverTime = _damageOverTime;
			
			staminaCost = _staminaCost;
			hpCost = _hpCost;
			hpGain = _hpGain;
			staminaGain = _staminaGain;
		}
		
		public function destroy():void
		{
			Starling.juggler.removeTweens(this);
		}
		
	}

}