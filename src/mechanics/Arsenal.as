package mechanics 
{
	/**
	 * ...
	 * @author Ace
	 */
	public class Arsenal 
	{
		public static const weapons:Vector.<Weapon> = new <Weapon>[
		
		//_                      	   dmgMin dmgMax  speedMin speedMax spreadAngle repeatedShots shotDelay cooldown, _bulletsPerShot, timeToLive, bulletType  _effects:Array)
			//hero weapons
			new Weapon("broomStick", 	100, 	  100,     50,      50,        1,             1,        0,       2,           1,             3,      fireball1          ),
			new Weapon("cleaner", 	    45, 	  55,       6,      6,        1,             1,        0,       0.4,           1,             3,      water1         ),     
			
			//criminal weapons
			new Weapon("revolver", 		10, 	  10,       4,      4,        1,             1,        0,       1.5,           1,             2,      bullet1          ),
			new Weapon("AK47",     		10, 	  10,       5,      5,       60,             3,        0.2,     4,           3,             3,      bullet1          ),
			
			//demon weapons
			new Weapon("fireWand", 		30, 	  30,       4,      6,       1,              1,        0,       2.5,           1,             2, 	  fireball1, [EffectLibrary.explosion]),
			new Weapon("demonFire",		20, 	  20,       4,      4,       360,            1,        0,       6,           12,            6,      iceBall          ),
			new Weapon("inferno",       10, 	  10,       3,      3,       360,             20,        0,    1000,           3,             6, 	  fireball3, [EffectLibrary.explosion]),
			
			//undead weapons
			new Weapon("poisonSpit", 	5, 	      5,       7,      7,        1,              8,        0.1,       2,           1,             2, 	  goo4, [EffectLibrary.poison]),
			new Weapon("poisonJar", 	5, 	      5,       3,      3,        1,              1,        0,       4,           1,             2, 	  goo2, [EffectLibrary.gooPuddle, EffectLibrary.explosion]),
			new Weapon("crowbar", 	    20, 	  20,      50,      50,        1,              1,        0,       4,           1,             2,      fireball1          ),
			
			//insect weapons
			new Weapon("flySpawner",    0, 	      0,      50,      50,        1,              1,        0,       6,           1,             2, 	  fireball1, [EffectLibrary.flySpawner]),
			new Weapon("wormSpawner",   0, 	      0,      50,      50,        1,              1,        0,       6,           1,             2, 	  fireball1, [EffectLibrary.wormSpawner]),
			new Weapon("venomSpit",     10, 	 10,      50,      50,        1,              1,        0,       6,           1,             2, 	  fireball1, [EffectLibrary.weakness]),
			
			//robot weapons
			new Weapon("mineSpawner",    0, 	  0,      50,      50,        1,              1,        0,       6,           1,             2, 	  fireball1, [EffectLibrary.mineSpawner]),
			new Weapon("rocketLauncher", 10,       10,      10,      10,        1,            1,        0,       1.5,           1,             2, 	  rocket1, [EffectLibrary.explosion]),
			new Weapon("stapleGun",      1,       1,      7,      7,        180,              1,        0,       5,           20,             1.5,      staple1     ),
			
			//monster weapons
			new Weapon("scream",         0, 	  0,      50,      50,        1,              1,        0,       6,           1,             2, 	  rocket1, [EffectLibrary.scream]),
			new Weapon("claw",           0,       0,      50,      50,        1,              1,        0,       6,           1,             2,      fireball1          ),
			new Weapon("stomp",          0,       0,      50,      50,        1,              1,        0,       6,           1,             2,      fireball1          ),
			
			
			
			new Weapon("sample2", 40, 50, 10, 20, 35, 0, 0, 0, 0, 0, 0, [EffectLibrary.explosion])
		];
		
		
		public static const iceBall:int = 0;
		
		public static const fireball1:int = 1;
		public static const fireball2:int = 2;
		public static const fireball3:int = 3;
		public static const fireball4:int = 4;
		
		public static const water1:int = 5;
		public static const water2:int = 6;
		
		public static const goo1:int = 7;
		public static const goo2:int = 8;
		public static const goo3:int = 9;
		public static const goo4:int = 10;
		
		public static const rocket1:int = 11;
		public static const rocket2:int = 12;
		
		public static const bullet1:int = 13;
		public static const bullet2:int = 14;
		
		public static const staple1:int = 15;
		public static const staple2:int = 16;
		
		public function Arsenal() 
		{
			
		}
		
		public static function getRandomWeapon():Weapon
		{
			return getWeapon(Constants.random.nextMinMax(0, weapons.length));
		}
		
		public static function getWeapon(id:int):Weapon
		{
			return weapons[id].clone();
		}
		
	}

}