package mechanics
{
	import flash.geom.Rectangle;
	import objects.Box;
	import objects.Bullet;
	import objects.Wall;
	import starling.core.Starling;
	import starling.events.EnterFrameEvent;
	
	/**
	 * ...
	 * @author Ace
	 */
	public class Collider
	{
		private var collidingObjects:Vector.<Box> = new <Box>[];
		
		private var wallVect:Vector.<Wall> = new <Wall>[];
		
		private static var _instance:Collider;
		
		public static function get instance():Collider
		{
			return _instance;
		}
		
		public function Collider()
		{
			_instance = this;
			
			/*var bounds1:Rectangle = image1.bounds;
			   var bounds2:Rectangle = image2.bounds;
			
			   if (bounds1.intersects(bounds2))
			 trace("Collision!");*/
			 
			Starling.current.stage.addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
		}
		
		private function onEnterFrame(e:EnterFrameEvent):void 
		{
			var object:Box;
			var object2:Box;
			
			var i:int, j:int;
			var colCount:int = collidingObjects.length;
			var colCount2:int = collidingObjects.length;
			
			for (i = 0; i < colCount; i++) {
				if (i<collidingObjects.length){
					object = collidingObjects[i];
				}
				colCount2 = collidingObjects.length;
				for (j = 0; j < colCount2; j++) {
					if (j<collidingObjects.length){
						object2 = collidingObjects[j];
					}
					if (doesCollide(object, object2))
					{
						object.onCollide(object2);
						object2.onCollide(object);
					}
				}
			}
			
			if (object && object.collidesWithWalls){
				var wallCount:int = wallVect.length;
				for (i = 0; i < wallCount; i++) {
					object2 = wallVect[i];
					if (doesCollide(object, object2))
					{
						object.onCollide(object2);
						object2.onCollide(object);
					}
				}			
			}
			
			/*for each (object in collidingObjects)
			{
				for each (object2 in collidingObjects)
				{
					if (doesCollide(object, object2))
					{
						object.onCollide(object2);
						object2.onCollide(object);
					}
				}
			}
			if (object.collidesWithWalls)
			{
				for each (object2 in wallVect)
				{
					if (doesCollide(object, object2))
					{
						object.onCollide(object2);
						object2.onCollide(object);
					}
				}
			}*/
		}
		
		public function isCollision(_bounds:Rectangle, object:Box):Boolean
		{
			var object2:Box;
			
			var i:int;
			var colCount:int = collidingObjects.length;
			for (i = 0; i < colCount; i++) {
				object2 = collidingObjects[i];
				if (doesCollideRect(object, object2, _bounds)) {
					return true;
				}
			}
			
			if (object.collidesWithWalls) {
				var wallCount:int = wallVect.length;
				for (i = 0; i < wallCount; i++) {
					object2 = wallVect[i];
					if (doesCollideRect(object, object2, _bounds)) {
						return true;
					}
				}				
			}
			
/*			for each (object2 in collidingObjects)
				if (doesCollideRect(object, object2, _bounds))
					return true;
			
			if (object.collidesWithWalls)
			{
				for each (object2 in wallVect)
					if (doesCollideRect(object, object2, _bounds))
						return true;
			}*/
			return false;
		}
		
		public function getCollision(_bounds:Rectangle, object:Box):Box
		{
			var object2:Box;
			
			var i:int;
			var colCount:int = collidingObjects.length;
			for (i = 0; i < colCount; i++) {
				object2 = collidingObjects[i];
				if (doesCollideRect(object, object2, _bounds)) {
					return object2;
				}
			}
			
			if (object.collidesWithWalls) {
				var wallCount:int = wallVect.length;
				for (i = 0; i < wallCount; i++) {
					object2 = wallVect[i];
					if (doesCollideRect(object, object2, _bounds)) {
						return object2;
					}
				}				
			}
			
			return null;
		}		
		
		private function doesCollideRect(object:Box, object2:Box, bounds1:Rectangle):Boolean 
		{
			if (object is Bullet && object2 is Bullet)
				return false;
				
			if (object.ignoreGroup == object2.group || object2.ignoreGroup == object.group)
				return false;
			
			if (object != object2 && object.solid && object2.solid)
			{
				var bounds2:Rectangle = object2.collisionBox;// bounds;
				
				if (bounds1.intersects(bounds2))
					return true;
			}
			
			return false;
		}
		
		private function doesCollide(object:Box, object2:Box):Boolean 
		{
			if (object is Bullet && object2 is Bullet)
				return false;
			
			if (object.ignoreGroup == object2.group || object2.ignoreGroup == object.group)
				return false;
			
			if (object != object2 && object.solid && object2.solid)
			{
				var bounds1:Rectangle = object.collisionBox;//bounds;
				var bounds2:Rectangle = object2.collisionBox;// bounds;
				
				if (bounds1.intersects(bounds2))
					return true;
			}
			
			return false;
		}
		
		public function add(object:Box):void
		{
			if (collidingObjects.indexOf(object) != -1)
				return;
			
			collidingObjects.push(object);
		}
		
		public function remove(object:Box):void {
			if (collidingObjects.indexOf(object) == -1)
				return;
			
			collidingObjects.splice( collidingObjects.indexOf(object), 1);
		}
		
		public function addWall(wall:Wall):void
		{
			if (wallVect.indexOf(wall) != -1)
				return;
			
			wallVect.push(wall);
		}
		
		public function reset():void {
			collidingObjects = new <Box>[];
			wallVect  = new <Wall>[];
		}
	
	}

}