package  
{
	import dragonBones.animation.WorldClock;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import gui.GoodJobWindow;
	import gui.GUI;
	import gui.TitleScreen;
	import gui.YouLoseWindow;
	import objects.Room;
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.utils.Color;
	import starling.utils.RectangleUtil;
	import starling.utils.ScaleMode;
	import util.Debugger;
	import util.Media;
	import util.Notification;
	import util.NotificationCenter;
	/**
	 * ...
	 * @author Ace
	 */
	public class Game extends Sprite
	{
		public static var instance:Game;
		
		public var UI:GUI;
		public var room:Room;
		
		public var waitingForVictory:Boolean = true;
		
		public function Game() 
		{
			super();
			
			instance = this;
			
			addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
			Starling.current.stage.addEventListener(Event.RESIZE, onResize);
			
			NotificationCenter.addOnce(this, Notification.assetsLoaded, onLoadFinished);
			new Media();
		}
		
		private function onLoadFinished():void 
		{
			Debugger.log("Assets Loaded");
			addChild( new TitleScreen(fadeToGame) );
		}
		
		private function fadeToGame():void {
			startGame();
			
			var blackQuad:Quad = new Quad(Constants.stageW, Constants.stageH, Color.BLACK);
			addChild(blackQuad);
			
			Starling.juggler.tween(blackQuad, 1.5, {
				alpha: 0.0,
				delay: 0.5,
				transition: Transitions.EASE_OUT,
				onComplete: blackQuad.removeFromParent
			});
		}
		
		public function startGame():void {
			if (room != null && room.parent != null) {
				room.removeFromParent(true);
			}
			
			room = new Room();
			addChild(room);
			
			if (UI != null) {
				setChildIndex(UI, numChildren - 1);
				return;
			}
			
			UI = new GUI();
			addChild(UI);
		}
		
		public function endGame(victory:Boolean):void {
			if (!waitingForVictory) {
				return;
			}
			waitingForVictory = false;
			
			if (victory){
				UI.addChild( new GoodJobWindow(Room.instance.reset) );
			} else {
				UI.addChild( new YouLoseWindow(startGame) );
			}
			
			
		}
		
		/**
		 * Fullskrīna menedžēšana
		 */
		private function onResize(event:starling.events.Event, size:Point):void
		{
			// varētu samazināt izšķirtspēju, tādejādi iegūstot ātrumu
			Debugger.log("0:Fullscreen. stagew: ", stage.stageWidth, " stageh: ", stage.stageHeight, " size:", size, " viewport:", Starling.current.viewPort);
			
			Constants.stageW = size.x;
			Constants.stageH = size.y;
			
			Constants.stageW = Constants.stageH * Constants.ratio;
			if (Constants.stageW > size.x)
			{
				Constants.stageW = size.x;
				Constants.stageH = Constants.stageW / Constants.ratio;
			}
			//koefX = size.x / 960;
			//koefY = size.y / 640;
			//koef = koefX < koefY ? koefX : koefY;
			
			RectangleUtil.fit(new Rectangle(0, 0, Constants.stageW, Constants.stageH), new Rectangle(0, 0, size.x, size.y),
				ScaleMode.SHOW_ALL, true, Starling.current.viewPort);
			
			trace("viewport: ", Starling.current.viewPort);
		}
		
		private function onEnterFrameHandler(e:EnterFrameEvent):void
		{
			WorldClock.clock.advanceTime(-1);
		}
	}

}