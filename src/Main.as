package
{
	import com.greensock.TweenMax;
	import feathers.system.DeviceCapabilities;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.system.Security;
	import starling.core.Starling;
	import util.Debugger;
	import util.NotificationCenter;
	
	/**
	 * Spēles sākumklase
	 * @author Ace
	 */
	public class Main extends Sprite
	{
		private var _starling:Starling;
		
		[Embed(source="../assets/intro.swf", mimeType='application/octet-stream')]
		public var intro1:Class;
		
		[Embed(source="../assets/Sounds/background.mp3")]
		public var backgroundMusic:Class;
		
		[Embed(source="../assets/Sounds/miscreations.mp3")]
		public var logoMusic2:Class;

		private var introMovie:Loader;
		
		private var music:Sound = new backgroundMusic(); 
		private var logoMusic:Sound = new logoMusic2();
		
		public function Main():void
		{
			trace("Main started");
			Security.allowDomain("*");
			Security.allowInsecureDomain("*");
			Security.allowInsecureDomain("localhost");
			
			if (stage)
			{
				init();
			}
			else
			{
				addEventListener(Event.ADDED_TO_STAGE, init);
			}
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			stage.stageFocusRect = false;
			
			
			addEventListener(MouseEvent.CLICK, onClick);
			
			//addChild(scene1);
			introMovie = new Loader();
			stage.frameRate = 24;
			Loader(addChild(introMovie)).loadBytes(new intro1());
			TweenMax.to(this, 8.875, { onComplete:endIntro } );
		}
		
		private function onClick(e:MouseEvent):void 
		{
			TweenMax.killTweensOf(this);
			endIntro();
		}
		
		private function endIntro():void 
		{
			removeChild(introMovie);
			introMovie = null;
			
			stage.frameRate = 60;
			
			music.play(0, 999);
			logoMusic.play(0);
			
			Starling.multitouchEnabled = false;  // useful on mobile devices
			
			_starling = new Starling(Game, stage);
			_starling.start();

			_starling.simulateMultitouch  = false;
            _starling.enableErrorChecking = false;
			
			//new Debugger();
			Debugger.register(Starling.current.nativeStage, true);
			Debugger.log("2:Client version: " + CONFIG::timeStamp);
			Debugger.activateTrigger();
			Debugger.notificationCenter = NotificationCenter;
			
			//Starling.current.showStatsAt("right");
			
			DeviceCapabilities.dpi = 326;
		}
		
		/*private function onResize(e:flash.events.Event):void
		{
			trace("Resizing!");
			if (!_starling)
				return;
			
			_starling.stage.stageWidth = stage.stageWidth;
			_starling.stage.stageHeight = stage.stageHeight;
		}*/
		
	}

}