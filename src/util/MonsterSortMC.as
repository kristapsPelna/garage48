package util 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import starling.display.DisplayObject;
	/**
	 * ...
	 * @author kamazs
	 */
	public class MonsterSortMC 
	{
		private static var prevDOs:Vector.<Object>;
		
		public function MonsterSortMC() 	{
		}
		
		public static function swapMC(mc:DisplayObject):void{
			if (!mc || !mc.parent)
				return;
			var mcy:int = getIndexY(mc);			
			if (mcy >= 0)
				mc.parent.setChildIndex(mc, mcy);	
		}
		
		private static function getIndexY(obj:DisplayObject):int{
			var idxY:int = obj.parent.getChildIndex(obj);
			var oldIdxY:int = idxY;
			var nextIdxY:int = idxY;
			for (var i:int = 0; i < obj.parent.numChildren; i++ )		{
				nextIdxY = isYBetween(obj, idxY);
				if ( nextIdxY < 0 )
					return idxY;
				else 
					idxY = nextIdxY;
			}
			return oldIdxY;
		}
		
		private static function isYBetween(obj:DisplayObject, idxY:int):int	{
			var idxYprev:int = idxY - 1;
			var idxYnext:int = idxY + 1;
			if (idxY < 0)	idxY = 0;
			if (idxYprev < 0)	idxYprev = 0;
			if (idxY > obj.parent.numChildren-1)	idxY = obj.parent.numChildren-1;
			if (idxYnext > obj.parent.numChildren-1)	idxYnext = -1;
			
			var curObj:DisplayObject = obj.parent.getChildAt(idxY);	
			var prevObj:DisplayObject = obj.parent.getChildAt(idxYprev);	
			var nextObj:DisplayObject = null;
			if (idxYnext > -1)
				nextObj = obj.parent.getChildAt(idxYnext);
			else
				nextObj = prevObj;
				
			if (idxYnext < 0) {
				if (obj.bounds.bottom < prevObj.bounds.bottom)
					return idxY - 1;
				else
					return -1;
			} else {
				if (obj.bounds.bottom >= prevObj.bounds.bottom && obj.bounds.bottom <= nextObj.bounds.bottom)
					return -1;
				else if (obj.bounds.bottom < prevObj.bounds.bottom)
					return idxY - 1;
				else 
					return idxY + 1;
			}
		}
	}
}