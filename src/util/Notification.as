package util
{
	public class Notification
	{
		public static const loadProgress:String = "loadProgress";
		
		public static const assetsLoaded:String = "assetsLoaded";
		public static const healthChange:String = "hpChange";
		public static const monsterHpChange:String = "monsterHpChange";
		
		
		public var name : String;
		public var object : Object;
		public var data : Object;

		/**
		Creates a new Notification object.
		@param inName: the name of the notification
		@param inObject: the notification object
		@param inData: the notification data
		 */
		public function Notification(inName : String, inObject : Object, inData : Object)
		{
			name = inName;
			object = inObject;
			data = inData;
		}
		
		public function toString() : String
		{
			return "Notification: name=" + name + "; object=" + object + "; data=" + data;
		}
	}
}