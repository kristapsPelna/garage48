package util 
{
	//import flash.text.TextFieldAutoSize;
	//import flash.text.TextFormat;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	/**
	 * ...
	 * @author 
	 */
	public class Fades extends Sprite
	{
		private var color:uint;		
		private var fade:Sprite;
		private var font:String;
		
		private var size:int = 20;
		private var icon:String = "";
		
		private var text:String;
		
		private var floatY:int = 0;// 10;
		private var totalTime:Number = 0.6;
		
		private static var iconArray:Array;
		
		public static var maxIconSize:int = 25;
		
		public static var defaultFont:String = "Arial Black";
		
		/**
		 * 
		 * @param	xx
		 * @param	yy
		 * @param	text
		 * @param	params - color:Number, delay:Number, icon:String, size:int = 20, totalTime:Number = 0.6, floatY:int = 0, font:String
		 */
		public function Fades(xx:int, yy:int, _text:String, params:Object = null) 
		{
			font = defaultFont;
			text = _text;
			var delay:Number = 0;
			
			x = xx;
			y = yy;
			
			touchable = false;
			
			if (params)
			{
				if (params.color)
					color = params.color;
				
				if (params.icon)
					icon = params.icon;
					
				if (params.size)
					size = params.size;
				
				if (params.totalTime)
					totalTime = params.totalTime;
					
				if (params.floatY)
					floatY = params.floatY;
					
				if (params.font)
					font = params.font;
					
				if (params.delay)
				{
					delay = params.delay;
					Starling.juggler.delayCall(showFade, delay);
					return;
				}
			}
			
			showFade();
		}
		
		public static function defineIcon(_name:String, _color:uint, _icon:Class):void
		{
			if (!iconArray)
				iconArray = [];
			
			iconArray[_name] = { color:_color, icon:_icon };
		}
		
		private function showFade():void
		{
			var textField:TextField = new TextField(1, 1, text, font, size, color);
			textField.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
			
			textField.x = -textField.width / 2;
			textField.y = -textField.height / 2;
			
			textField.hAlign = HAlign.CENTER;  // horizontal alignment
			textField.vAlign = VAlign.CENTER; // vertical alignment
			
			/*if (iconArray && iconArray[icon])
			{
				lb.x -= maxIconSize / 2;
				maxIconSize = lb.textHeight;
				addIcon(icon, lb.x + lb.width + 2);
			}*/

			//lb.textColor = color;
			
			scaleX = scaleY = 0;
			//textField.pivotX = textField.width / 2;
			//textField.pivotY = textField.height / 2;
			addChild(textField);
			
			var newY:Number = this.y - floatY;
			
			var tween:Tween = new Tween(this, totalTime * 0.4);
			tween.moveTo(this.x, newY);
			tween.scaleTo(1);
			
			var tween2:Tween = new Tween(this, totalTime * 0.5);
			tween2.delay = tween.totalTime;
			tween2.fadeTo(0);
			
			Starling.juggler.add(tween);
			Starling.juggler.add(tween2);
			Starling.juggler.delayCall(remove, totalTime);
		}
		
		private function addIcon(name:String, xx:int):void
		{
			color = iconArray[name].color;
			
			var icon:Sprite = new iconArray[name].icon();
			
			//resize to maxIconSize
			var divisor:Number = ( icon.width >= icon.height) ? icon.width / maxIconSize : icon.height / maxIconSize;
			icon.width = Math.floor(icon.width / divisor);
			icon.height = Math.floor(icon.height / divisor);
			
			icon.x = xx + icon.width / 2;
			
			fade.addChild(icon);
		}
		
		private function remove():void
		{
			removeChild(fade);
			fade = null;
			if (parent)
				parent.removeChild(this);
		}
		
	}
}