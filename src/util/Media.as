package util
{
	import com.greensock.events.LoaderEvent;
	import flash.system.Capabilities;
	import starling.utils.AssetManager;
	
	/**
	 * ...
	 * @author Ace
	 */
	public class Media extends AssetManager
	{
		private static var _instance:Media;
		
		//[Embed(source = "/../assets/ElegantIcons.ttf", embedAsCFF="false", fontFamily="ElegantIcons")]
		//public static const ElegantIcons:Class;
		
		private static var appTextures:Array = [];
		
		public static const atlasDictionary:Array = [];
		
		private static var playerImages:Vector.<Object> = new <Object>[];
		
		private static const atlasNames:Object = {
			//"items":"itemAtlas78"
		}

		public static function get instance():Media
		{
			return _instance;
		}
		
		public function Media()
		{
			_instance = this;
			
			super(1, false);
			
			verbose = Capabilities.isDebugger;
			
			enqueue(EmbeddedAssets);
			
			//enqueue(Config.filePath + "atlas/" + atlasNames["items"] + ".xml" + "?v=" + Math.random()*1000);
			//enqueue(Config.filePath + "atlas/" + atlasNames["items"] + ".png" + "?v=" + Math.random()*1000);
			
			//loadAtlas(Config.filePath + "atlas/" + atlasNames["items"] + ".xml");
			loadQueue(onProgress);
		}
		
		private function onProgress(ratio:Number):void
		{
			NotificationCenter.post(Notification.loadProgress, { percent:ratio * 100 } );
			
			if (ratio == 1)
			{
				NotificationCenter.post(Notification.assetsLoaded);
			}
		}
		
		private function errorHandler(event:LoaderEvent):void
		{
			Debugger.log("error occured with " + event.target + ": " + event.text);
		}
	
	}

}