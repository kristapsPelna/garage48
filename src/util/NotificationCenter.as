﻿package util
{
	
	public class NotificationCenter
	{
		private static const DEFAULT_NOTIFICATION_NAME:String = "__0__";
		
		private static var mObservers:Object = {};
		/**< Key value object with notification name as key and an array as value; the array stores a value object with the properties observer, method, note and object. */
		private static var mObjects:Object = {};
		/**< For optimized object retrieval in post; key value object with object name as key and an array as value; the array stores NotificationObserverData objects */
		private static var mTempCleanupList:Array = [];
		/**< Marked observerData objects to be removed */
		public static var checkDuplicates:Boolean = false;

		
		public function NotificationCenter()
		{
			/*mObservers = {};
			mObjects = {};
			mTempCleanupList = [];
			setCheckOnAdding(false);*/
		}
		
		/**
		   Registers inObserver to receive notifications with the name inNotificationName and/or containing inNotificationObject.
		   When a notification of name inNotificationName containing the object inNotificationObject is posted, inObserver's method inMethod is called with a {@link Notification} as the argument. If inNotificationName is undefined, the notification center notifies the observer of all notifications with an object matching inNotificationObject. If inNotificationObject is nil, the notification center notifies the observer of all notifications with the name inNotificationName. inObserver may not be undefined.
		   @param inMethod : The observer's method that will be called when sent a notification. This method should only have one argument (of type Notification).
		   @param inNotificationName : (optional) notification identifier name; if undefined, you must use inNotificationObject
		   @param inNotificationObject : (optional) notification identifier object; the notification center notifies the observer of all notifications with an object matching this object
		   @example
		 */
		public static function addOnce(inObserver:Object, inNotificationName:String, inMethod:Function):void
		{
			var callOnce:Boolean = true;
			
			add(inObserver, inNotificationName, inMethod, null, callOnce);
		}
		
		public static function add(inObserver:Object, inNotificationName:String, inMethod:Function, inNotificationObject:Object = null, callOnce:Boolean = false):void
		{
			inNotificationName ||= DEFAULT_NOTIFICATION_NAME;
			// dummy because empty string cannot be searched on
			if (inMethod == null)
			{
				return;
			}
			
			if (mObservers[inNotificationName] == undefined)
			{
				mObservers[inNotificationName] = new Array();
			}
			// check if alreay in list with the same arguments
			if (checkDuplicates && contains(mObservers, inObserver, inMethod, inNotificationName, inNotificationObject))
			{
				return;
			}
			var observerData:NotificationObserverData = new NotificationObserverData(inObserver, inMethod, inNotificationName, inNotificationObject, callOnce);
			mObservers[inNotificationName].push(observerData);
			
			// optimize object handling, to retrieve all messages targeted to the object
			if (inNotificationObject != null)
			{
				if (checkDuplicates && contains(mObjects, inObserver, inMethod, inNotificationName, inNotificationObject))
				{
					// No warning, should be covered by previous warning message.
					return;
				}
				if (mObjects[inNotificationObject] == undefined)
				{
					mObjects[inNotificationObject] = new Array();
				}
				mObjects[inNotificationObject].push(observerData);
			}
		}
		
		/**
		   Removes inObserver as the observer of notifications with the name inNotificationName and object inNotificationObject from the object. inObserver may not be nil. Be sure to invoke this method before removing the observer object or any object specified in addObserver.
		   If inNotificationName is nil, inObserver is removed as an observer of all notifications containing inNotificationObject. If inNotificationObject is nil, inObserver is removed as an observer of inNotificationName containing any object.
		   @param inNotificationName: (optional) notification identifier name; if undefined, you must use inNotificationObject
		   @param inNotificationObject: (optional) notification identifier object; specify when the observer listens to notifications with an object matching this object
		   @example
		 */
		public static function remove(inObserver:Object, inNotificationName:String = "", inNotificationObject:Object = null):void
		{
			var removed:Boolean;
			if (inNotificationName == "")
			{
				//if inNotificationName is "", then remove all listeners for this observer
				removed = removeFromCollection(mObservers, inObserver, null, inNotificationObject);
			}
			else
			{
				inNotificationName ||= DEFAULT_NOTIFICATION_NAME;
				
				if (mObservers[inNotificationName] == null)
				{
					return;
				}
				
				removed = removeFromCollection(mObservers, inObserver, inNotificationName, inNotificationObject);
			}
			
			if (inNotificationObject != null)
			{
				removed = removeFromCollection(mObjects, inObserver, inNotificationName, inNotificationObject);
			}
			
			// erase marked observerData objects
			var len:Number = mTempCleanupList.length;
			if (len > 0)
			{
				var i:Number = len;
				while (--i != -1)
				{
					removeObject(mTempCleanupList[i]);
				}
				mTempCleanupList = [];
			}
		}
		
		public static function post(inNotificationName:String = "", inData:Object = null):void
		{
			postIn(inNotificationName, null, inData);
		}
		
		/**
		   Creates a {@link Notification} instance and passes this to the observers associated through inNotificationName or inNotificationObject.
		   @param inNotificationName : (optional) notification identifier name; if undefined, you must use inNotificationObject
		   @param inNotificationObject : (optional) notification identifier object; typically the object posting the notification; may be null; if not null, any message is sent that is directed to this object
		   @param inData : (optional) object to pass - this will be packed in the Notification
		   @example
		   This example finds all observers that are associated with the notification name 'ButtonDidUpdateNotification', and passes them a Notification object with data "My message".
		   <code>
		   NotificationCenter.getDefaultCenter().post("ButtonDidUpdateNotification", null, "My message");
		   </code>
		   The following example sends a notification to the observers that are associated with identifier object <i>anIdentifier</i>. Note that the name of the notification is not important when you pass an object - it may be an empty string. The object is associated with the observer in addObserver (<i>the notification center notifies the observer of all notifications with an object matching inNotificationObject</i>).
		   <code>
		   var anIdentifier:Object = this;
		   NotificationCenter.getDefaultCenter().post(null, anIdentifier, "My message");
		   </code>
		 */
		public static function postIn(inNotificationName:String = "", inNotificationObject:Object = null, inData:Object = null):void
		{
			inNotificationName ||= DEFAULT_NOTIFICATION_NAME;
			
			var observers:Array;
			if (inNotificationObject == null)
			{
				observers = mObservers[inNotificationName];
			}
			else
			{
				observers = mObjects[inNotificationObject];
			}
			
			if (observers == null || observers.length == 0)
			{
				return;
			}
			
			var len:uint = observers.length;
			var i:int = len;
			while (--i != -1)
			{
				var observerData:NotificationObserverData = NotificationObserverData(observers[i]);
				if (inNotificationObject != observerData.object)
				{
					continue; // skip
				}
				
				if (inData)
					observerData.method(new Notification(observerData.note, inNotificationObject, inData)); //sending notification only if there were extra data
				else
					observerData.method();
				// We are now calling the method directly
				// Another implementation might be to pass the Notification object to the NotificationCenter class and do the calling in another method.

				if (observerData.callOnce)
					remove(observerData.observer, observerData.note, observerData.object);
			}
		}
		
		// PRIVATE METHODS
		/**
		   Loops through the collection inCollection to check if the item is already present. This is very time consuming with large collections.
		   @return True when in list, false when not.
		 */
		private static function contains(inCollection:Object, inObserver:Object, inMethod:Function, inNotificationName:String, inNotificationObject:Object):Boolean
		{
			for (var n:String in inCollection)
			{
				var o:Object = inCollection[n];
				if (o == null)
				{
					return false;
				}
				var len:uint = o.length;
				var i:int = len;
				while (--i != -1)
				{
					var observerData:NotificationObserverData = o[i];
					if (observerData.isEqualToParams(inObserver, inMethod, inNotificationName, inNotificationObject))
					{
						return true;
					}
				}
			}
			return false;
		}
		
		/**
		
		 */
		private static function removeFromCollection(inCollection:Object, inObserver:Object, inNotificationName:String, inNotificationObject:Object):Boolean
		{
			var removed:Boolean = false;
			for (var n:String in inCollection)
			{
				var o:Object = inCollection[n];
				if (o == null)
					continue;
				var len:uint = o.length;
				for (var i:int = len - 1; i >= 0; --i)
				{
					var observerData:Object = o[i];
					if (observerData && observerData.observer == inObserver)
					{
						if (inNotificationObject != null && inNotificationName != null)
						{
							if (observerData.object == inNotificationObject && observerData.note == inNotificationName)
							{
								mTempCleanupList.push(observerData);
								if (len == 1)
								{
									inCollection[n] = null;
								}
								else
									o = o.splice(i, 1);
								removed = true;
							}
						}
						
						if (inNotificationObject != null && inNotificationName == null)
						{
							// check for object
							if (observerData.object == inNotificationObject)
							{
								mTempCleanupList.push(observerData);
								if (len == 1)
								{
									inCollection[n] = null;
								}
								else
									o = o.splice(i, 1);
								removed = true;
							}
						}
						
						if (inNotificationObject == null && inNotificationName != null)
						{
							// check for notification name
							if (observerData.note == inNotificationName)
							{
								mTempCleanupList.push(observerData);
								if (len == 1)
								{
									inCollection[n] = null;
								}
								else
									o = o.splice(i, 1);
								
								removed = true;
							}
						}
						
						// else
						if (inNotificationObject == null && inNotificationName == null)
						{
							mTempCleanupList.push(observerData);
							if (len == 1)
							{
								inCollection[n] = null;
							}
							else
								o = o.splice(i, 1);
							removed = true;
						}
					}
				}
			}
			return removed;
		}
		
		/**
		   Deletes an object with its properties.
		 */
		private static function removeObject(o:Object):void
		{
			if (o == null)
				return;
			for (var n:String in o)
			{
				delete o[n];
				o[n] = null;
			}
			// delete o;
			o = null;
		}
		
		public function toString():String
		{
			return "NotificationCenter";
		}
	}
}

class NotificationObserverData
{
	public var observer:Object;
	public var method:Function;
	public var note:String;
	public var object:Object;
	public var callOnce:Boolean;
	
	public function NotificationObserverData(inObserver:Object, inMethod:Function, inNote:String, inObject:Object, _callOnce:Boolean = false)
	{
		observer = inObserver;
		method = inMethod;
		note = inNote;
		object = inObject;
		callOnce = _callOnce;
	}
	
	/**
	   Tests if the variables of the current NotificationObserverData object is equal to the passed parameters.
	 */
	public function isEqualToParams(inObserver:Object, inMethod:Function, inNote:String, inObject:Object):Boolean
	{
		if (observer === inObserver && method === inMethod && note == inNote && object === inObject)
		{
			return true;
		}
		return false;
	}
	
	public function toString():String
	{
		return "NotificationObserverData: " + "method=" + method + ";note=" + note + "; object=" + object;
	}
}