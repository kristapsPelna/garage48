package util 
{
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.system.System;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.utils.setTimeout;
	 
	public class Debugger extends Sprite
	{
		private var _edText:TextField;
		private var _edCommand:TextField;
		private var _scrollbar:MovieClip;
		private var _resizeLine:Sprite;
		
		private var isScrolling:Boolean = false;
		
		private var sliderHeight:int;
		private const scrollWid:int = 10;
		private	const inputHeight:int = 20;
		
		private var myTextFormat:TextFormat = new TextFormat();
		
		private static var isTraceEnabled:Boolean = true;
		
		private static var maxLines:int;
		private static var maxLineHistory:int;

		private static const _commands:Array = [];
		private static const _default_commands:Array = [];
		
		private static const _lastCommands:Vector.<String> = new <String>[];
		private static var _lastCmdNum:int;
		
		private static var _debugs:Vector.<String> = new <String>[];
		public static var debugStage:Stage;
		
		private static var maxWidth:int;
		private static var maxHeight:int;
		
		public static var notificationCenter:Object;
		
		private static var _panel:Debugger;
		
		/**
		 * Colors
		 */
		private const bg_color : uint = 0x172125;// 0x293134;
		private const text_color : uint = 0xFFFFFF;
		private const scrollBack_color : uint = 0x164556;
		private const sliderBack_color : uint = 0x172125;
		private const buttonBack_color : uint = 0x172125;
		private const buttonSymbol_color : uint = 0x164556;
		private const inputBackground_color : uint = 0x164556;// 0xCCDCE2;
		private const inputText_color : uint = 0xFFFFFF;// 0x000000;
		
		private static const textColors:Vector.<String> = new <String>[
			"#888888", //0:
			"#FFFFFF", //1:
			"#FFFF00", //2:
			"#FF0000", //3:
			"#00FF00", //4:
			"#2288FF"  //5:
		];
		
		private static function show():void
		{
			if (_panel || !debugStage) 
				return;

			_panel = new Debugger();
			debugStage.addChild(_panel);

			/*CONFIG::FLEX {
				if (_panel._edText)
					System.setClipboard(_panel._edText.text);
			}*/
		}
		private static function hide():void
		{
			if (!_panel) return;
			
			if (_panel.parent)
				_panel.parent.removeChild(_panel);//debugStage.removeChild(_panel);
			_panel = null;
		}
		
		public function Debugger()
		{
			if (!debugStage) {
				trace("Debugger: Tev vajag definēt debug stage!");
				return;
			}
			
			create();
		}
		
		private function create():void 
		{
			//Memo field
			_edText = new TextField();
			_edText.multiline = true;
			_edText.wordWrap = true;
			
			maxWidth = debugStage.stageWidth;
			_edText.width = maxWidth - scrollWid;
			_edText.height = maxHeight;
			_edText.textColor = text_color;
			_edText.backgroundColor = bg_color;
			_edText.background = true;
			_edText.alwaysShowSelection = true;
			
			_edText.mouseWheelEnabled = true;
			_edText.setTextFormat(myTextFormat);
			
			var j:int = _debugs.length - maxLines;
			if (j < 0)
				j = 0;
			for (var i:int = j; i < _debugs.length; i++)
				addString(_debugs[i], false);
			
			_edText.addEventListener(Event.SCROLL, onTextScroll);

			//Scrollbar 
			var shape:Shape;
			var sp:Sprite;
			
			_scrollbar = new MovieClip();
				//Background
				shape = new Shape();
				shape.graphics.beginFill(scrollBack_color, 1);
				shape.graphics.drawRect(0, 12, scrollWid, maxHeight - 12 * 2);
				shape.graphics.endFill();
				
				sp = new Sprite();
				sp.addChild(shape);
				_scrollbar.back = _scrollbar.addChild(sp);
			
				//UP button
				sp = new Sprite();
				shape = new Shape();
					shape.graphics.beginFill(buttonBack_color, 1);
					shape.graphics.drawRect(0, 0, scrollWid, 12);
					shape.graphics.endFill();
					shape.graphics.beginFill(buttonSymbol_color, 1);
					shape.graphics.drawTriangles(Vector.<Number>([1, scrollWid-1, scrollWid/2, 1, scrollWid-1, scrollWid-1]));
					shape.graphics.endFill();
				sp.addChild(shape);
					
				_scrollbar.btnup = _scrollbar.addChild(sp);
				_scrollbar.btnup.addEventListener(MouseEvent.MOUSE_DOWN, onUpClick);
					
				//DOWN button
				sp = new Sprite();
				shape = new Shape();
					shape.graphics.beginFill(buttonBack_color, 1);
					shape.graphics.drawRect(0, 0, scrollWid, 12);
					shape.graphics.endFill();
					shape.graphics.beginFill(buttonSymbol_color, 1);
					shape.graphics.drawTriangles(Vector.<Number>([1, 1, scrollWid/2, scrollWid-1, scrollWid-1, 1]));
					shape.graphics.endFill();
				sp.addChild(shape);
				
				_scrollbar.btndn = _scrollbar.addChild(sp); 
				_scrollbar.btndn.addEventListener(MouseEvent.MOUSE_DOWN, onDnClick);
				_scrollbar.btndn.y = _scrollbar.height;
				
				//Slider
				sliderHeight = 10;
				sp = new Sprite();
				shape = new Shape();
					shape.graphics.beginFill(sliderBack_color, 1);
					shape.graphics.drawRect(1, -10, scrollWid - 2, 20);
					shape.graphics.endFill();
				sp.addChild(shape);
				_scrollbar.slider = _scrollbar.addChild(sp);
				_scrollbar.slider.mouseChildren = false;
				_scrollbar.slider.mouseEnabled = false;
				_scrollbar.slider.y = 13;
				
				_scrollbar.back.addEventListener(MouseEvent.MOUSE_DOWN, onSliderDown);
			
			_scrollbar.x = _edText.width;

			_edText.scrollV = _edText.numLines;
			onTextScroll(null);
			
			myTextFormat.color = inputText_color;
			//Command line input field
			_edCommand = new TextField();
			_edCommand.selectable = true;
				_edCommand.type = TextFieldType.INPUT;
				_edCommand.height = inputHeight;
				_edCommand.width = _edText.width + scrollWid;
				_edCommand.y = maxHeight;
				
				_edCommand.backgroundColor = inputBackground_color;
				_edCommand.background = true;
				
				_edCommand.defaultTextFormat = myTextFormat;
				
				_edCommand.addEventListener(KeyboardEvent.KEY_DOWN, onCommandKey);
				setFocus();
				
			//resize line
				_resizeLine = new Sprite();
				shape = new Shape();
					shape.graphics.beginFill(bg_color, 1);
					shape.graphics.drawRect(0, 0, maxWidth, 1);
				_resizeLine.addChild(shape);
				_resizeLine.y = maxHeight + inputHeight;
			
			debugStage.addEventListener(Event.ADDED, childAdded);
			addEventListener(Event.REMOVED_FROM_STAGE, remove);
			
			addChild(_resizeLine);
			addChild(_edText);
			addChild(_scrollbar);
			addChild(_edCommand);
		}
		
		private function childAdded(e:Event):void 
		{
			debugStage.addChild(_panel);
		}
		
		public function setFocus():void
		{
			debugStage.focus = _edCommand;
		}
		
		private function onTextScroll(e:Event):void
		{
			if (!_edText)
				return;
			//move slider
			if (_edText.maxScrollV == 1) {
				var k:Number = 0;
				sliderHeight = _scrollbar.height - 28 + 4;
			} else {
				k = (_edText.scrollV - 1)  / (_edText.maxScrollV - 1);
				sliderHeight = (_scrollbar.height - 28) / _edText.maxScrollV;
				sliderHeight *= 2;
			}
			if (sliderHeight < 10)
				sliderHeight = 10;
			_scrollbar.slider.height = sliderHeight - 4;
			
			
			var pos:Number = k * (maxHeight - _scrollbar.btndn.height * 2 - sliderHeight) + sliderHeight / 2 + _scrollbar.btndn.height;
			_scrollbar.slider.y = pos;
		}
		
		private function onSliderDown(e:MouseEvent):void
		{
			e.currentTarget.removeEventListener(MouseEvent.MOUSE_DOWN, onSliderDown);
			addEventListener(MouseEvent.MOUSE_MOVE, onSliderMove);
			addEventListener(MouseEvent.ROLL_OUT, onStopClick);
			addEventListener(MouseEvent.MOUSE_UP, onStopClick);
			
			isScrolling = true;
			onSliderMove(null);
		}
		
		private function onSliderMove(e:MouseEvent):void
		{
			if (!isScrolling) return;
			if (e) {
				e.stopPropagation();
				e.stopImmediatePropagation();
			}
			
			var pos:Number = ((_scrollbar.mouseY - _scrollbar.btndn.height) / _scrollbar.back.height) * _edText.maxScrollV;
			_edText.scrollV = Math.floor(pos);
		}
		
		private function doScroll(vS:int):void
		{
			if (!isScrolling) return;
			
			_edText.scrollV += vS;
			setTimeout(doScroll, 100, vS);
		}
		
		private function onStopClick(e:MouseEvent):void
		{
			_scrollbar.back.addEventListener(MouseEvent.MOUSE_DOWN, onSliderDown);
			removeEventListener(MouseEvent.MOUSE_MOVE, onSliderMove);
			removeEventListener(MouseEvent.ROLL_OUT, onStopClick);
			removeEventListener(MouseEvent.MOUSE_UP, onStopClick);
			isScrolling = false;
		}
		
		private function onDnClick(e:MouseEvent):void
		{
			addEventListener(MouseEvent.ROLL_OUT, onStopClick);
			addEventListener(MouseEvent.MOUSE_UP, onStopClick);
			
			_edText.scrollV++;
			isScrolling = true;
			setTimeout(doScroll, 500, 1);
		}
		
		private function onUpClick(e:MouseEvent):void
		{
			addEventListener(MouseEvent.ROLL_OUT, onStopClick);
			addEventListener(MouseEvent.MOUSE_UP, onStopClick);
			
			_edText.scrollV--;
			isScrolling = true;
			setTimeout(doScroll, 500, -1);
		}
		
		private function remove(e:Event):void
		{
			debugStage.removeEventListener(Event.ADDED, childAdded);
			removeEventListener(Event.REMOVED_FROM_STAGE, remove);
			
			removeChild(_resizeLine);
			removeChild(_edText);
			removeChild(_scrollbar);
			removeChild(_edCommand);
			
			_scrollbar.btndn.removeEventListener(MouseEvent.MOUSE_DOWN, onDnClick);
			_scrollbar.btnup.removeEventListener(MouseEvent.MOUSE_DOWN, onDnClick);
			_scrollbar.back.removeEventListener(MouseEvent.MOUSE_DOWN, onSliderDown);
			removeEventListener(MouseEvent.MOUSE_MOVE, onSliderMove);
			removeEventListener(MouseEvent.ROLL_OUT, onStopClick);
			removeEventListener(MouseEvent.MOUSE_UP, onStopClick);

			_edCommand.removeEventListener(KeyboardEvent.KEY_DOWN, onCommandKey);	
			_edCommand = null;
			_scrollbar = null;
			_edText    = null;
		}
		
		private function addString(vString:String, refresh:Boolean = true):void
		{
			var split:Array = vString.split('\n');
			for each (var sp:String in split)
			{
				var color:int = 1;
				
				if (vString.substr(1, 1) == ":" && int(vString.substr(0, 1)) < textColors.length)
				{
					color = int(vString.substr(0, 1));
					sp = sp.substr(2);
				}
				
				if (_edText)
					_edText.htmlText += "<font color='" + textColors[color] + "' size='13'><b>" + sp + '</b></font>';
			}
			
			if (refresh && _edText)
				_edText.scrollV = _edText.numLines;
		}
		
		private function cmdPost(rest:Array = null):void
		{
			if (!notificationCenter)
				return;
			
			if (rest.length < 1)
				return;
			
			var message:String = String(rest[0]);
			var data:Object;
			
			if (rest.length > 1)
				data = { data:rest[1] };
			
			notificationCenter.post(message, data);
		}
		
		private function cmdHelp(rest:Array = null):void
		{
			if (!_edText) return;
			
			for (var cmd:Object in _default_commands)
				if (_default_commands[cmd].help)
					addString("5:  " + cmd + " - " + _default_commands[cmd].help, false );
			
			//if (_commands.length > 0) { //length not available for object
				addString("5: Custom:", false );
				
				for (cmd in _commands)
					if (_commands[cmd].help)
						addString("5:  " + cmd + " - " + _commands[cmd].help, false );
			//}
			
			_edText.scrollV = _edText.numLines;
		}
		
		private function cmdCls(rest:Array = null):void
		{
			_edText.text = "";
			_edText.scrollV = 0;
		}
			
		private function cmdDelete(rest:Array = null):void
		{
			cmdCls();
			_debugs = new Vector.<String>();
		}
		
		private function cmdClose(rest:Array = null):void
		{
			trigger();
		}
		
		private function cmdResize(rest:Array = null):void
		{
			if (rest.length > 0)
				var height:int = int(rest[0]);
			else
				height = 150;

			if (height < 50)
				height = 50;
				
			if (height > debugStage.stageHeight)
				height = debugStage.stageHeight - inputHeight;
			
			maxHeight = height;
			remove(null);
			create();
			
			addString("Resize Debugger to " + height);
		}
		
		private function cmdShowHistory(rest:Array = null):void
		{
			cmdCls();
			for (var i:int = 0; i < maxLineHistory; i++)
				addString(_debugs[i], false);
				
			_edText.scrollV = _edText.numLines;
			addString("5:Showing History");
		}
		
		private function onCommandKey(e:KeyboardEvent):void
		{
			if (e.charCode == 13 && _edCommand.text)
			{
				var s:String = _edCommand.text;
				var cmd:Array = s.split(' ');
				
				if (!_commands[cmd[0]] && !_default_commands[cmd[0]])
					return;
					
				_edCommand.text = "";
				
				if (_default_commands[cmd[0]] && _default_commands[cmd[0]].callback)
				{
					var cmdName:String = cmd[0];
					cmd.shift();
					
					if (cmd.length > 0) {
						addString("5:" + cmdName + " " + cmd);
						this[_default_commands[cmdName].callback](cmd);
					} else {
						addString("5:" + cmdName);
						this[_default_commands[cmdName].callback]();
					}
				}
				else
				{
					cmdName = cmd[0];
					cmd.shift();

					if (_commands[cmdName] && _commands[cmdName].callback)
					{
						addString("5:" + cmdName + " " + cmd);
						if (cmd.length > 0)
							_commands[cmdName].callback(cmd);
						else 
							_commands[cmdName].callback();
					}
				}
				
				_lastCommands.push(s);
				_lastCmdNum = _lastCommands.length;
				
			}
			else if (e.keyCode == 38) //up
			{
				if (_lastCommands.length > 0) {
					if (_lastCmdNum > _lastCommands.length)
						_lastCmdNum = _lastCommands.length;
					if (_lastCmdNum < 1)
						_lastCmdNum = 1;
					
					_edCommand.text = _lastCommands[_lastCmdNum - 1];
					_lastCmdNum--;
				}
				
				//_edCommand.setSelection(_edCommand.text.length, _edCommand.text.length); //place caret at end of text
			}
			else if (e.keyCode == 40) //down
			{
				if (_lastCommands.length > 0)
				{
					if (_lastCmdNum > _lastCommands.length)
						_lastCmdNum = _lastCommands.length;
					if (_lastCmdNum < 1)
						_lastCmdNum = 1;
					
					_edCommand.text = _lastCommands[_lastCmdNum - 1];
					_lastCmdNum++;
				}
				
				//_edCommand.setSelection(_edCommand.text.length, _edCommand.text.length); //place caret at end of text
			}
			
		}
		
		private static function onStageKey(e:KeyboardEvent):void 
		{
			if (e.ctrlKey && e.keyCode == 192) {
				Debugger.trigger();
				return;
			}
		}
		/**
		 * 
		 * @param	vStage - main stage
		 * @param	height - default Debugger height
		 * @param	_maxLines - maximum lines to show
		 * @param	_maxLineHistory - maximum lines to keep in history
		 */
		public static function register(_stage:Stage, _traceEnabled:Boolean, _height:int = 180, _maxLines:int = 100, _maxLineHistory:int = 200):void
		{
			debugStage = _stage;
			maxWidth = debugStage.stageWidth;
			
			if (_height < 50)
				_height = 50;
				
			if (_height > debugStage.stageHeight)
				_height = debugStage.stageHeight;
				
			isTraceEnabled = _traceEnabled;
			maxHeight = _height;
			
			maxLines = _maxLines;
			maxLineHistory = _maxLineHistory;
			
			registerDefaultCommand("/help", "cmdHelp", "Help command");
			registerDefaultCommand("/cls", "cmdCls", "Clear debug screen");
			registerDefaultCommand("/delete", "cmdDelete", "Delete messages");
			registerDefaultCommand("/resize", "cmdResize", "Resize Debugger height");
			registerDefaultCommand("/history", "cmdShowHistory", "Show all messages in history");
			registerDefaultCommand("/exit", "cmdClose", "Close Debugger");
			registerDefaultCommand("/post", "cmdPost", "Post Notification to NotificationCenter");
		}
		
		/**
		 * Activate Debugger to trigger on KeyboardEvent
		 */
		public static function activateTrigger():void
		{
			if (debugStage)
				debugStage.addEventListener(KeyboardEvent.KEY_DOWN, onStageKey);
		}
		/**
		 * Deactivate Debugger to trigger on KeyboardEvent
		 */
		public static function removeTrigger():void
		{
			if (debugStage)
				debugStage.removeEventListener(KeyboardEvent.KEY_DOWN, onStageKey);
		}
		
		/**
		 * 
		 * @param	... rest - any params to be printed out
		 */
		public static function log(... rest):void
		{
			if (rest.length < 1)
				return;
			
			var vString:String = "";
			for (var i:uint = 0; i < rest.length; i++)
				vString += rest[i] + " ";
			
			_debugs.push(vString);

			if (isTraceEnabled)
				trace(vString);
			
			while (_debugs.length > maxLineHistory) _debugs.shift();
			
			if (_panel) _panel.addString(vString);
		}
	
		/**
		 * Show/Hide debuger panel
		 */
		public static function trigger():void
		{
			if (_panel) hide();
			else show();
		}
		
		/**
		 * register new command
		 * @param	vCommand command name
		 * @param	fName function name
		 * @param	vHelp help string
		 */
		private static function registerDefaultCommand(vCommand:String, fName:String, vHelp:String = ""):void
		{		
			_default_commands[vCommand] = {
				callback:fName,
				help:vHelp
			};
		}
	
		/**
		 * register new command
		 * @param	vCommand command name
		 * @param	_function - function name
		 * @param	vHelp help string
		 */
		public static function registerCommand(vCommand:String, _function:Function, vHelp:String = ""):void
		{		
			if (_commands[vCommand]) {
				_commands[vCommand].callback = _function;
				_commands[vCommand].help = vHelp;
				return;
			}
			
			_commands[vCommand] = {
				callback:_function,
				help:vHelp
			};
		}
		/**
		 * Register new color shema
		 * @param	vSubstr keyword to find - e.g. 0: or 1:
		 * @param	vColor color in hex #FF00FF
		 */
		public static function registerColor(vSubstr:String, vColor:String):void
		{
			textColors[vSubstr] = vColor;
		}
	}

}