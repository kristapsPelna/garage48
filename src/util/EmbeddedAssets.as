package util 
{
	/**
	 * ...
	 * @author Ace
	 */
	public class EmbeddedAssets 
	{
		/*[Embed(source = "/../assets/assets.png", mimeType="image/png")]
		public static const assets:Class;
		
		[Embed(source = "/../assets/assets.xml", mimeType="application/octet-stream")]
		public static const assetsXML:Class;*/
		
		[Embed(source="../../gfx/mainAtlas.png", mimeType="image/png")]
		public static const mainAtlas:Class;
		
		[Embed(source="../../gfx/mainAtlas.xml", mimeType="application/octet-stream")]
		public static const mainAtlasXML:Class;
		
		[Embed(source="../../assets/hero.png", mimeType="image/png")]
		public static const hero:Class;
		
		[Embed(source="../../assets/hero.xml", mimeType="application/octet-stream")]
		public static const heroXML:Class;
		
		[Embed(source="../../assets/miscAssets.png", mimeType="image/png")]
		public static const miscAssets:Class;
		
		[Embed(source="../../assets/miscAssets.xml", mimeType="application/octet-stream")]
		public static const miscAssetsXML:Class;
		
		public function EmbeddedAssets() 
		{
			
		}
		
	}

}