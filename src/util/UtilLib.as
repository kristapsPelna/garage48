package util 
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.net.SharedObject;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author Ace
	 */
	public class UtilLib
	{
		
		public function UtilLib() {
		
		}
		
		public static function nearestPow2(aSize:int):int
		{
			return Math.pow( 2, Math.round( Math.log( aSize ) / Math.log( 2 ) ) ); 
		}
		
		public static function getDayCount(timeStamp:uint):int
		{
			var currDate:Date = new Date(timeStamp*1000); //timestamp_in_seconds*1000 - if you use a result of PHP time function, which returns it in seconds, and Flash uses milliseconds
			/*D = currDate.getDate();
			M = currDate.getMonth()+ 1; //because Returns the month (0 for January, 1 for February, and so on)
			Y = currDate.getFullYear();*/
			
			var d:Date = new Date(currDate.getFullYear(), currDate.getMonth()+ 1, 0);
			return d.getDate();
		}
		
		public static function scaleBitmapData(bitmapData:BitmapData, scale:Number):BitmapData
		{
			if (!bitmapData)
				return null;
				
			if (scale == 1)
				return bitmapData;
			
            scale = Math.abs(scale);
            var width:int = (bitmapData.width * scale) || 1;
            var height:int = (bitmapData.height * scale) || 1;
            var transparent:Boolean = bitmapData.transparent;
            var result:BitmapData = new BitmapData(width, height, transparent, 0x00000000);
            var matrix:Matrix = new Matrix();
            matrix.scale(scale, scale);
            result.draw(bitmapData, matrix);
            return result;
        }
		
		public static function twoLineIntersectionPoint(x1start:Number,  y1start:Number, x1end:Number, y1end:Number, x2start:Number, y2start:Number, x2end:Number, y2end:Number, as_seg:Boolean = true):Point
		{
		    var a1:Number = y1end - y1start;
		    var b1:Number = x1start - x1end;
		    var c1:Number = x1end * y1start - x1start * y1end;
		    var a2:Number = y2start - y2end;
		    var b2:Number = x2end - x2start;
		    var c2:Number = x2start * y2end - x2end * y2start;
		 
		    var denom:Number = a1 * b2 - a2 * b1;
		    if (denom == 0)
		    	return null;
		   	
		    var ip:Point = new Point( (b1*c2 - b2*c1) / denom, (a2*c1 - a1*c2) / denom );
		 
		    //---------------------------------------------------
		    //Do checks to see if intersection to endpoints
		    //distance is longer than actual Segments.
		    //Return null if it is with any.
		    //---------------------------------------------------
		    if (as_seg) {
		        if(Math.pow(ip.x - x1end, 2) + Math.pow(ip.y - y1end, 2) > Math.pow(x1start - x1end, 2) + Math.pow(y1start - y1end, 2))
		           return null;
		        
		        if(Math.pow(ip.x - x1start, 2) + Math.pow(ip.y - y1start, 2) > Math.pow(x1start - x1end, 2) + Math.pow(y1start - y1end, 2))
		           return null;
		 
		        if(Math.pow(ip.x - x2start, 2) + Math.pow(ip.y - y2start, 2) > Math.pow(x2end - x2start, 2) + Math.pow(y2end - y2start, 2))
		           return null;
		        
		        if(Math.pow(ip.x - x2end, 2) + Math.pow(ip.y - y2end, 2) > Math.pow(x2end - x2start, 2) + Math.pow(y2end - y2start, 2))
		           return null;
		    }
		    return ip;
		}
		
		public static function pointToLineDist(pointX:int, pointY:int, lineP1x:int, lineP1y:int, lineP2x:int, lineP2y:int):Number
		{
			//now... get the point on the line closest to our point
			var newPoint:Point = pointLineIntersect(pointX, pointY, lineP1x, lineP1y, lineP2x, lineP2y);
			
			//now get the distance between the points.. this is your answer
			return Math.pow(pointX - newPoint.x, 2) + Math.pow(pointY - newPoint.y, 2);
		}
		
		public static function pointLineIntersect(pX:int, pY:int, lineP1x:int, lineP1y:int, lineP2x:int, lineP2y:int):Point
		{
			//get the normalized vector of the line
			var norm:Point = new Point(lineP2x - lineP1x, lineP2y - lineP1y);
			var mag:Number = Math.sqrt(norm.x*norm.x + norm.y*norm.y);
			norm.x /= mag;
			norm.y /= mag;
			
			//now project your point on the line
			var pointVector:Point = new Point(pX - lineP1x, pY - lineP1y);
			var dotProduct:Number = pointVector.x*norm.x + pointVector.y*norm.y;
			
			//now... get the point on the line closest to our point
			return new Point(norm.x*dotProduct + lineP1x, norm.y*dotProduct + lineP1y);
		}
		
		public static function getDuration(x1:Number,  y1:Number, x2:Number, y2:Number, pixelsPerSecond:int):Number
		{
			var value:Number = (Math.sqrt(twoPointDistanceSquared(x1, y1, x2, y2)) / pixelsPerSecond);
			return value;
		}
		
		public static function twoPointDistanceSquared(x1:Number,  y1:Number, x2:Number, y2:Number):Number
		{
		    var dx:Number = x1-x2;
		    var dy:Number = y1-y2;
		    return dx * dx + dy * dy;
		}
		
		/*public static function getCollisionPoint(set1a:Point, set1b:Point, set2a:Point, set2b:Point):Point
		{
		    var E:Number = set1b.y - set1a.y;
		    var F:Number = set1a.x - set1b.x;
		    var G:Number = set1a.x * set1b.y - set1a.y * set1b.x;
		     
		    var P:Number = set2b.y - set2a.y;
		    var Q:Number = set2a.x - set2b.x;
		    var R:Number = set2a.x * set2b.y - set2a.y * set2b.x;
		     
		    var denominator:Number = (E * Q - P * F);

			return new Point((G * Q - R * F) / denominator, (R * E - G * P) / denominator);
		}*/
		
		public static function degrees(radians:Number):Number
		{
		    return radians * 180/Math.PI;
		}
		 
		public static function radians(degrees:Number):Number
		{
		    return degrees * Math.PI / 180;
		}
		
		public static function angleBetween(x1:Number, y1:Number, x2:Number, y2:Number, radians:Boolean = false):Number
        {
			var angle:Number = Math.atan2( y2 - y1, x2 - x1 );
			if (radians == true) {
            	return angle;
			} else {
				angle *= (180 / Math.PI);
				return angle < 0 ? angle + 360 : angle;
			}
       	}
		
		public static function convertToHHMMSS($seconds:Number):String
		{
			var s:Number = $seconds % 60;
			var m:Number = Math.floor(($seconds % 3600 ) / 60);
			var h:Number = Math.floor($seconds / (60 * 60));
			 
			var hourStr:String = (h == 0) ? "" : doubleDigitFormat(h) + ":";
			var minuteStr:String = doubleDigitFormat(m) + ":";
			var secondsStr:String = doubleDigitFormat(s);
			 
			return hourStr + minuteStr + secondsStr;
		}
		
		public static function print_r(obj:*, level:int = 0, output:String = ""):* 
		{
			var tabs:String = "";
			
			for(var i:int = 0; i < level; i++) 
				tabs += "\t";
			
			if (typeof obj == "object" || typeof obj == "array")
			{
				for(var child:* in obj)
				{
					output += tabs +"["+ child +"] => "+ obj[child];
				
					var childOutput:String = UtilLib.print_r(obj[child], level+1);
					if(childOutput != '') output += ' {\n'+ childOutput + tabs +'}';
					
					output += "\n";
				}
			}
			else
			{
				output += '" ' + obj + ' \n"';
			}
			
			return output;
		}
		
		public static function getLevelExp(level:int):int
		{
			//return 100 + 100 * level * (level - 1);
			var expArr:Vector.<int> = new <int>[0, 300, 900, 1800, 3000];
			if (level < 6)
			{
				//trace("test level", level, expArr[level - 1]);
				return expArr[level - 1];
			}
			
			var exp:int = Math.pow((level - 4), 1.2) * 2000 + 100;
			exp -= exp % 100;
			//trace("test level", level, exp);
			return exp;
		}
		
		public static function printf(vString:String, vReplace:Object, vDelimit:String = "%"):String 
		{
			for (var key:String in vReplace) {
				var myPattern:RegExp = new RegExp(vDelimit + key + vDelimit, "g");
				
				vString = vString.replace(myPattern, vReplace[key]);
				//trace(key + ": " + vString);  
			}
			return vString;
		}
		
		public static function capitalizeFirst(str:String):String
		{
			var firstChar:String = str.substr(0, 1);
			var restOfString:String = str.substr(1, str.length);
			
			return firstChar.toUpperCase()+restOfString.toLowerCase();
		}
		
		public static function verticalAlignTextField(tf:TextField):void
		{
			tf.y += Math.round((tf.height - tf.textHeight) / 2);
		}
		
		public static function resize(mc:Object, maxDimension:int):Object
		{
			//already under the maxDimension
			if (mc.width <= maxDimension && mc.width <= maxDimension)
				return mc;
			else {
				var divisor:Number = ( mc.width >= mc.height) ? mc.width / maxDimension : mc.height / maxDimension;
				mc.width = Math.floor(mc.width / divisor);
				mc.height = Math.floor(mc.height / divisor);
			}

			return mc;
		}
		
		private static function doubleDigitFormat(num:uint):String
		{
			if (num < 10)
				return ("0" + num);
			
			return String(num);
		}
		
		public static function getStringHourTime(seconds:Number, showSeconds:Boolean = true, showDays:Boolean = false):String
		{
			var s:int = seconds % 60;
			var m:int = Math.floor((seconds % 3600 ) / 60);
			var h:int = Math.floor(seconds / (60 * 60));
			var d:int = Math.floor(h / 24);
			
			if (showDays)
				var hourStr:String = (h == 0) ? "" : doubleDigitFormat(h - d * 24) + ":";
			else
				hourStr = (h == 0) ? "" : doubleDigitFormat(h) + ":";
			
			var minuteStr:String = doubleDigitFormat(m);
			var secondsStr:String = doubleDigitFormat(s);
			
			var result:String = "";
			if (showDays && d > 0)
				result += d + "d. ";
			
			result += hourStr + minuteStr;
			
			if (showSeconds)
				result += ":" + secondsStr;
			
			return result;
		}
		
		public static function bubbleSort(toSort:Array, param:String = "y"):Array
		{
			var changed:Boolean = false;
			var i:int;
		 
			while (!changed)
			{
				changed = true;
		 
				for (i = 0; i < toSort.length - 1; i++)
				{
					if (toSort[i][param] > toSort[i + 1][param])
					{
						var tmp:Object = toSort[i];
						toSort[i] = toSort[i + 1];
						toSort[i + 1] = tmp;
		 
						changed = false;
					}
				}
			}
		 
			return toSort;
		}
		
		public static function scaleTextField(label:TextField, vert:Boolean = false, verticalAlign:Boolean = true):void
		{
			var tWidth:int;
			var lWidth:int = label.width;
			if (vert)
				lWidth = label.height;
				
			var textHeight:Number = label.textHeight;
			var _caption:String = label.text;
			var fieldFormat:TextFormat = label.getTextFormat();// getTextFormat();
		  
			var myTextField:TextField = new TextField();
			myTextField.text = _caption;
			myTextField.embedFonts = label.embedFonts;
			
			/*if (myFormat)
				fieldFormat.letterSpacing = myFormat.letterSpacing;*/
			
			myTextField.setTextFormat(fieldFormat);
			myTextField.autoSize = TextFieldAutoSize.LEFT;
			
			tWidth = myTextField.width;
			if (vert)
				tWidth = myTextField.height;
			
			while (tWidth > lWidth)
			{
				fieldFormat.size = int(fieldFormat.size) - 1;
				myTextField.setTextFormat(fieldFormat);
				
				tWidth = myTextField.width;
				//trace(fieldFormat.size, _caption, " width:", lWidth, " textWidth:", tWidth, " newWidth:", label.width, " newText:", label.textWidth);
			}
			
			label.setTextFormat(fieldFormat);
			if (verticalAlign)
				label.y += (textHeight - label.textHeight) / 2;
		}
		
		public static function formatNumber(number:Number,vDelimit:String = " "):String
		{
			var numString:String = number.toString();
			var result:String = '';
			
			while (numString.length > 3)
			{
				var chunk:String = numString.substr( -3);
				numString = numString.substr(0, numString.length - 3);
				result = vDelimit + chunk + result;
			}
			
			if (numString.length > 0)
				result = numString + result;
			
			return result;
		}
		
		public static function trimWhitespace($string:String):String 
		{
			if ($string == null)
				return "";
			
			return $string.replace(/^\s+|\s+$/g, "");
		}
		
		public static function colorText(label:TextField, symbols:int, color:Number = 0xFFFFFF):void 
		{
			var format:TextFormat = new TextFormat();
			format.color = color;
			label.setSelection(label.length-symbols, label.length);
			label.setTextFormat(format, label.selectionBeginIndex, label.selectionEndIndex);
			
			label.setSelection(0, 0);
		}
		
		public static function getSharedObject(uid:String, key:String):Object 
		{
			var sharedObj:SharedObject = SharedObject.getLocal(key + uid, "/");
			//nextDebugger.addInfo("getSharedObject " + key + uid + " " + JSON.stringify(mySharedObject.data));
			sharedObj.flush();
			return sharedObj;
		}
		
		public static function setSharedObject(uid:String, key:String, param:String, val:*):void 
		{
			var sharedObj:SharedObject = SharedObject.getLocal(key + uid, "/");
			sharedObj.data[param] = val;
			//nextDebugger.addInfo("setSharedObject " + key + uid + " " + JSON.stringify(mySharedObject.data));
			sharedObj.flush();
		}
		
		public static function swapMC(mc:DisplayObject):void
		{
			if (!mc)
				return;
				
			var mcy:int = getIndexY(mc);
			if (mcy >= 0)
				mc.parent.setChildIndex(mc, mcy);	
		}
		
		private static function getIndexY(obj:DisplayObject):int
		{
			var idxY:int = obj.parent.getChildIndex(obj);
			var oldIdxY:int = idxY;
			var nextIdxY:int = idxY;
			for (var i:int = 0; i < obj.parent.numChildren; i++ )
			{
				nextIdxY = isYBetween(obj, idxY);
				if ( nextIdxY < 0 )
					return idxY;
				else 
					idxY = nextIdxY;
			}
			return oldIdxY;
		}
		
		private static function isYBetween(obj:DisplayObject, idxY:int):int
		{
			var idxYprev:int = idxY - 1;
			var idxYnext:int = idxY + 1;
			if (idxY < 0)	idxY = 0;
			if (idxYprev < 0)	idxYprev = 0;
			if (idxY > obj.parent.numChildren-1)	idxY = obj.parent.numChildren-1;
			if (idxYnext > obj.parent.numChildren-1)	idxYnext = -1;
			
			if (idxYnext > -1)
				var nextObj:DisplayObject = obj.parent.getChildAt(idxYnext);
			//var curObj:DisplayObject = obj.parent.getChildAt(idxY);	
			var prevObj:DisplayObject = obj.parent.getChildAt(idxYprev);	
			
			if (idxYnext < 0)
			{
				if (obj.y <prevObj.y)
					return idxY - 1;
				else
					return -1;
			}
			else
			{
				if (obj.y >= prevObj.y && obj.y < nextObj.y)
					return -1;
				else if (obj.y <prevObj.y)
					return idxY - 1;
				else 
					return idxY + 1;
			}
		}
	}
}