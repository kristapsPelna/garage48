package util
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import Main;
	import userData.PlayerData;
	
	/**
	 * @author Ace
	 */
	public class GlobalTimer
	{
		public static var dispatcher:EventDispatcher = new EventDispatcher();
		
		private static var timer:Timer = new Timer(250);
		
		private static var count:int = 0;
		public static var avgSecCounter:int = 0;
		private static var avgSecTime:int = 0; //average time for a second to pass
		
		public static var minSecTime:int = 0; //minimum time for a second to pass
		public static var maxSecTime:int = 0; //maximum time for a second to pass
		private static var lastSecTime:int = 0;
		
		public static var serverTime:int;
		public static var lastPingTime:int = 0;
		
		private static var player:PlayerData = Main.player;
		
		public static var gameActive:Boolean = false;
		
		private static var overheadTime:int;
		
		public function GlobalTimer()
		{
			lastPingTime = serverTime;
			lastSecTime = new Date().getTime();
			timer.addEventListener(TimerEvent.TIMER, onTimeTick);
			timer.start();
		}
		
		public static function resetAverageValues():void
		{
			avgSecCounter = 0;
			avgSecTime = 0;
		}
		
		public static function getAverageSecTime():Number
		{
			return Math.round(avgSecTime / avgSecCounter);
		}
		
		public static function resetTimePassValues():void
		{
			minSecTime = 0;
			maxSecTime = 0;
		}
		
		private function onTimeTick(e:TimerEvent):void 
		{
			count++;
			var timePassed:int = new Date().getTime() - lastSecTime;
			
			if (timePassed > 999) //flash second passed
				dispatcher.dispatchEvent(new Event(Notification.GLOBAL_TIMER));
			
			if (timePassed + overheadTime > 999 || count == 4) //real second passed
			{
				overheadTime += timePassed - 999;
				
				if (gameActive)
				{
					avgSecTime += timePassed;
					avgSecCounter += 1;
				}
				
				if (timePassed > maxSecTime)
					maxSecTime = timePassed;
				
				if (timePassed < minSecTime || minSecTime == 0)
					minSecTime = timePassed;
				
				lastSecTime = new Date().getTime();
				
				serverTime++;

				count = 0;

				dispatcher.dispatchEvent(new Event(Notification.PLAY_TIMER));
			}
		}
		
		public static function remove():void
		{
			timer.stop();
		}
	}
}
