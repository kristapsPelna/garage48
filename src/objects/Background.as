package objects 
{
	import flash.display.Bitmap;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import util.Media;
	/**
	 * ...
	 * @author Ace
	 */
	public class Background extends Sprite
	{
		private var image:Image;
		
		public static const rugStore:String = "rugStore";
		
		[Embed(source="/../assets/background_1.png")]
		private static const backgroundImage:Class;
		
		public static const laundry:String = "laundry";
		
		[Embed(source="/../assets/velasmazgatuve.png")]
		private static const backgroundImageLaundry:Class;
		private var textures:Vector.<starling.textures.Texture>;
		
		public function Background() 
		{
			
			textures = new Vector.<Texture>();
			textures.push( Texture.fromBitmapData(new backgroundImage().bitmapData) );
			textures.push( Texture.fromBitmapData(new backgroundImageLaundry().bitmapData) );
			
			image = new Image(textures[0]);
					addChild(image);
		}
		
		public function show(type:int = 0):void		{
			image.texture =  textures[type];
		}
		
	}

}