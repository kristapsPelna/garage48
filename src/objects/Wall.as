package objects 
{
	import flash.geom.Rectangle;
	import starling.display.Quad;
	/**
	 * ...
	 * @author Ace
	 */
	public class Wall extends Box
	{
		
		public function Wall(_width:int, _height:int) 
		{
			super();
			collisionBox = new Rectangle(x, y, _width, _height);
			name = group = "wall";
			addQuad(_width, _height);
		}
		
		private function addQuad(_width:int, _height:int):void 
		{
			var quad:Quad = new Quad(_width, _height, 0xFF0000);
			quad.alpha = 0.5;
			addChild(quad);
		}
		
	}

}