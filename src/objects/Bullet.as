package objects 
{
	import flash.geom.Point;
	import mechanics.Arsenal;
	import mechanics.Weapon;
	import starling.core.Starling;
	import starling.display.Image;
	import util.Media;
	import util.UtilLib;
	/**
	 * ...
	 * @author Ace
	 */
	public class Bullet extends Box
	{
		private var weapon:Weapon;
		
		//private var dmg:Number;
		private var flySpeed:Number;
		
		private var location:Point;
		
		private var angle:Number;
		
		//angle in radians
		public function Bullet(_weapon:Weapon, _angle:Number, _location:Point) 
		{
			name = "bullet";
			walkThrough = true;
			
			collidesWithWalls = false;
			
			angle = _angle;
			weapon = _weapon;
			location = _location;
			
			switch (weapon.bulletType)
			{
				case Arsenal.fireball1:
				//case Arsenal.fireball2:
				//case Arsenal.fireball3:
				//case Arsenal.fireball4:
					explosionType = "fire";
					break;
				case Arsenal.goo1:
				case Arsenal.goo2:
				case Arsenal.goo3:
				case Arsenal.goo4:
					explosionType = "goo";
					break;
				case Arsenal.rocket1:
				case Arsenal.rocket2:
					explosionType = "grenade";
					break;
			}
			
			super();
			
			maxSpeed = Constants.random.nextMinMax(weapon.speedMin, weapon.speedMax);
			
			dmg = Constants.random.nextMinMax(weapon.dmgMin, weapon.dmgMax);
			//flySpeed = Constants.random.nextMinMax(weapon.speedMin, weapon.speedMax) / 60;
			
			var xDist:Number = Math.sin(angle) * flySpeed;
			var yDist:Number = Math.cos(angle) * flySpeed;
		}
		
		override public function create():void 	{
			super.create();
			addImage();
		}
		
		override public function onUpdate():void 
		{
			
			moveAtAngle(angle);
			
			if (!Constants.screenRect.containsPoint(pos)) {
				destroy();
			}
			
			super.onUpdate();
		}
		
		private function addImage():void 
		{
			var names:Array = ["fireball", "fireball1", "fireball2", "fireball3", "fireball4", "water1", "water2", "goo1", "goo2", "goo3", "goo4", "rocket1", "rocket2", "bullet1", "bullet2", "skava01", "skava02"];
			
			var textureName:String = names[weapon.bulletType];
			
			var image:Image = new Image(Media.instance.getTexture(textureName));
			
			image.rotation = angle + UtilLib.radians(180);
			addChild(image);
		}
		
		override public function onCollide(box:Box):void 
		{
			super.onCollide(box);
			if (box is Wall) {
				destroy();
			}
			//destroy();
		}
		
	}

}