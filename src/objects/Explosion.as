package objects 
{
	import mechanics.Collider;
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.events.Event;
	import util.Media;
	/**
	 * ...
	 * @author Ace
	 */
	public class Explosion extends Box
	{
		private var explosion:MovieClip
		
		public function Explosion(explosionType:String) 
		{
			switch (explosionType)
			{
				case "fire":
					explosion = new MovieClip(Media.instance.getTextures("blow_fireball"), 24);
					dmg = 10;
					break;
				case "grenade":
					explosion = new MovieClip(Media.instance.getTextures("blow_granade-rocket"), 24);
					dmg = 10;
					break;
				case "goo":
					explosion = new MovieClip(Media.instance.getTextures("blow_green"), 24);
					dmg = 10;
					break;
				case "boss":
					explosion = new MovieClip(Media.instance.getTextures("blow_boss"), 24);
					//dmg = 10;
					break;
				case "water":
					explosion = new MovieClip(Media.instance.getTextures("boom_water"), 24);
					break;
			}
			
			if (!explosion)
				return;
				
			
			explosion.addEventListener(Event.COMPLETE, onExplosionEnd);
			explosion.play();
			
			explosion.x -= explosion.width * 0.5;
			explosion.y -= explosion.height * 0.5;
			Starling.juggler.add(explosion);
			addChild(explosion);
			Collider.instance.add(this);
		}
		
		private function onExplosionEnd(e:Event):void 
		{
			explosion.removeEventListener(Event.COMPLETE, onExplosionEnd);
			explosion = null;
			
			this.parent.removeChild(this);
		}
		
		override public function destroy():void 
		{
			super.destroy();
			Collider.instance.remove(this);
		}
		
	}

}