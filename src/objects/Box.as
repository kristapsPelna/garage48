package objects 
{
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.engine.TextBlock;
	import flash.utils.Timer;
	import mechanics.Collider;
	import mechanics.Effect;
	import mechanics.Resistance;
	import monster.Animation;
	import starling.animation.DelayedCall;
	import starling.animation.IAnimatable;
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.Color;
	import util.Fades;
	import util.Media;
	import util.MonsterSortMC;
	
	/**
	 * Spēles pamatobjekts
	 * @author Kamazs
	 */
	public class Box extends Sprite
	{
		public var group:String = "";
		public var ignoreGroup:String;
		public static var idx:int;
		public var id:int;
		
		public var explosionType:String;
		
		public var type:String;
		public var speed:Point = new Point(0, 0);
		public var maxSpeed:Number = 5;
		public var acceleration:Number = 0;
		public var solid:Boolean = true;
		public var animation:Animation;
		public var maxHP:Number = 1;
		public var hp:Number = 1;
		public var hpRegen:Number = 0;
		public var attackType:Vector.<Effect>;
		public var effects:Vector.<Effect>;
		public var stamina:Number = 1;
		public var maxStamina:Number = 1;
		public var staminaRegen:Number = 0;
		public var resistances:Vector.<Resistance>;
		public var produceRate:Number = 0;
		public var produceType:int;
		public var detectionRange:Number = 0;
		public var timeToLive:Number = 0;
		public var referenceBox:Box;
		public var pos:Point;
		public var dmg:Number = 0;
		public var walkThrough:Boolean = false;
		public var bossScale:Number = 0.25; // 1/4 no 1.0
		
		public var collisionBox:Rectangle;
		public var collidesWithWalls:Boolean = true;
		
		private var speedMod:Number = 0.0;
		private var speedModResetCall:DelayedCall;
		private var attackSpeedMod:Number = 0.0;
		private var attackSpeedModResetCall:DelayedCall;
		private var regenHpKoefMod:Number = 0.0;
		private var regenHpKoefModResetCall:DelayedCall;
		private var staminaRegenKoefMod:Number = 0.0;
		private var staminaRegenKoefModResetCall:DelayedCall;
		private var damageOverTimeCall:IAnimatable;
		
		private var safePosition:Point;
		private var clock:Timer;
		private var target:Point;
		private var touched:Boolean = false;
		private var nextCollisionBox:Rectangle;
		private var damaged:Boolean = false;
		
		
		public function Box() {
			super();
			
			safePosition = new Point(x, y);
			pos = safePosition.clone();
			target = new Point();
			id = idx++;
			
			clock = new Timer(1000);
			clock.addEventListener(TimerEvent.TIMER, heartBeat);
			clock.start();
			
			create();
			reset();
			
			addEventListener(Event.ENTER_FRAME, enterFrame);
		}
		
		public function addExplosion():void
		{
			if (explosionType == null || explosionType == "")
				return;
			
			var explosion:Explosion = new Explosion(explosionType);
			explosion.x = this.x;
			explosion.y = this.y;
			Room.instance.add(explosion);
			Room.instance.add(new Fades(x + width * 0.5, y + height * 0.5, "BOOM!"));
		}
		
		public function create():void
		{
			
		}
		
		public function reset():void {
			collisionBox = bounds.clone();
			if (collisionBox.width>0 && collisionBox.height>0){
				//addQuad(bounds.width, bounds.height);
			}
			
			nextCollisionBox = bounds.clone();
		}
		
		private function addQuad(_width:int, _height:int):void 
		{
			var quad:Quad = new Quad(_width, _height, 0xFF0000);
			quad.alpha = 0.2;
			addChild(quad);
		}
		
		public function heartBeat(e:TimerEvent):void 		{
			setHP(hp + hpRegen);
			setStamina(stamina + staminaRegen);
			damaged = false;
		}
		
		public function setHP(hp:Number):void {
			this.hp = hp;
			if (hp > maxHP) {
				this.hp = maxHP;
			}
			if (hp < 0) {
				this.hp = 0;
				destroy();
			}
		}
		
		public function setStamina(stamina:Number):void {
			this.stamina = stamina;
			if (stamina > maxStamina) {
				this.stamina = maxStamina;
			}
			if (stamina < 0) {
				this.stamina = 0;
			}			
		}
		
		public function applyDamage(dmg:Number):void 	{
			setHP(hp - dmg);
			
			if (dmg>0){
				addChild( new Fades(width / 2, -5, dmg.toString(), { color: Color.RED, floatY: 50 } ));
			}
		}
		
		// ################## MODI #####################
		
		public function applyMod(mod:Number, time:Number, delayCall:DelayedCall) :void {
			if (delayCall != null) {
				Starling.juggler.remove(delayCall);
				delayCall.reset(resetMod, time);
			} else {
				delayCall = Starling.juggler.delayCall(resetMod, time, delayCall) as DelayedCall;
			}
		}		
		
		public function resetMod(delayCall:DelayedCall):void {
			Starling.juggler.remove(speedModResetCall);
		}
		
		// ############################################
		
		public function setAnimation(anim:Animation):void {
			if (animation != null) {
				if (animation.parent != null) {
					animation.removeFromParent();
				}
			}
			
			animation = anim;
			
			if (animation!=null){
				addChild(animation);
			}
		}
		
		public function moveTowardsBox(box:Box):void {
			var tX:Number = box.x + (box.width - width) / 2;
			var tY:Number = box.y + (box.height - height) / 2;//box.y + box.height;
			
			moveTowards(tX, tY);		
		}
		
		public function moveAwayFromBox(box:Box):void {
			var tX:Number = box.x + box.width / 2;
			var tY:Number = box.y + box.height;
			
			moveAway(tX, tY);		
		}
		
		public function moveAtAngle(dir:Number):void {
			/*speed.x = maxSpeed * Math.cos (dir);
			speed.y = maxSpeed * Math.sin (dir);

			x += speed.x * (1.0 + speedMod);
			y += speed.y * (1.0 + speedMod);	
			collisionBox.x = x;
			collisionBox.y = y;*/
			moveInAngle(dir);
		}
		
		public function moveTowards(toX:Number, toY:Number):void {
			var angle:Number = Math.atan2(y - toY, x - toX);
			moveInAngle(angle);
			/*speed.x = -maxSpeed * Math.cos (angle);
			speed.y = -maxSpeed * Math.sin (angle);

			x += speed.x * (1.0 + speedMod);
			y += speed.y * (1.0 + speedMod);	
			collisionBox.x = x;
			collisionBox.y = y;	*/		
		}
		
		public function moveAway(toX:Number, toY:Number):void {
			var angle:Number = Math.atan2(y - toY, x - toX);
			moveInAngle( Math.PI-angle);
			/*speed.x = -maxSpeed * Math.cos (angle);
			speed.y = -maxSpeed * Math.sin (angle);
			
			x -= speed.x * (1.0 + speedMod);
			y -= speed.y * (1.0 + speedMod);	
			collisionBox.x = x;
			collisionBox.y = y;	*/
		}
		
		public function moveInAngle(dir:Number):void {
			var collider:Box = Collider.instance.getCollision(collisionBox, this);
			if (collider!=null && !collider.walkThrough) {
				return;
			}
			speed.setTo(maxSpeed, maxSpeed);
			nextCollisionBox.copyFrom(collisionBox);
			nextCollisionBox.x += -speed.x * Math.cos(dir) * (1.0 + speedMod);
			nextCollisionBox.y += -speed.y * Math.sin(dir) * (1.0 + speedMod);
			
			collider = Collider.instance.getCollision(nextCollisionBox, this);
			if (collider!=null && !collider.walkThrough) {
				var dX:Number = nextCollisionBox.x - collisionBox.x;
				var dY:Number = nextCollisionBox.y - collisionBox.y;
				x -= 0.5 * dX/Math.abs(dX);
				y -= 0.5 * dY/Math.abs(dY);
				if (Collider.instance.isCollision(nextCollisionBox, this)) {
					speed.setTo(maxSpeed, maxSpeed);
				} else {
					speed.setTo(0,0);
				}
				
				return;
			} 	
			x += -speed.x *  Math.cos(dir) * (1.0 + speedMod);
			y += -speed.y *  Math.sin(dir) * (1.0 + speedMod);	
			collisionBox.x = x;
			collisionBox.y = y;
		}		
		
		public function moveInDirection(dirX:int, dirY:int):void {
			var collider:Box = Collider.instance.getCollision(collisionBox, this);
			if (collider!=null && !collider.walkThrough) {
				return;
			}
			speed.setTo(maxSpeed, maxSpeed);
			nextCollisionBox.copyFrom(collisionBox);
			nextCollisionBox.x += speed.x * dirX * (1.0 + speedMod);
			nextCollisionBox.y += speed.y * dirY * (1.0 + speedMod);
			
			collider = Collider.instance.getCollision(nextCollisionBox, this);
			if (collider!=null && !collider.walkThrough) {
				var dX:Number = nextCollisionBox.x - collisionBox.x;
				var dY:Number = nextCollisionBox.y - collisionBox.y;
				x -= dX;
				y -= dY;
				if (Collider.instance.isCollision(nextCollisionBox, this)) {
					speed.setTo(maxSpeed, maxSpeed);
				} else {
					speed.setTo(0,0);
				}
				
				return;
			} 	
			x += speed.x * dirX * (1.0 + speedMod);
			y += speed.y * dirY * (1.0 + speedMod);	
			collisionBox.x = x;
			collisionBox.y = y;
		}
		
		public function accelerate(spd:Point):void {
			if (Math.abs(spd.x) >= maxSpeed && Math.abs(spd.y) >= maxSpeed) {
				return;
			} else {
				spd.x += acceleration;
				spd.y += acceleration;
			}
		}
		
		public function apply(eff:Effect):void	{
			if (eff.maxDMG > 0) {
				applyDamage(Constants.random.nextMinMax(eff.minDMG, eff.maxDMG));
			}
			
			if (eff.moveSpeedKoef > 0) {
				applyMod(eff.moveSpeedKoef, eff.duration, speedModResetCall);
			}
				
			if (eff.attackSpeedKoef > 0) {
				// TO BE ADDED LATER
			}
				
			if (eff.regenHPKoef > 0) {
				applyMod(eff.regenHPKoef, eff.duration, regenHpKoefModResetCall);
			}
			
			if (eff.staminaRegenKoef > 0) {
				applyMod(eff.staminaRegenKoef, eff.duration, staminaRegenKoefModResetCall);
			}
			
			if (eff.damageOverTime > 0) {
				if (damageOverTimeCall != null) {
					Starling.juggler.remove(damageOverTimeCall);
					damageOverTimeCall = null;
				}
				damageOverTimeCall = Starling.juggler.repeatCall(applyDamage, eff.stepInterval, eff.stepCount, eff.damageOverTime);
			}
		}
		
		public function destroy():void 	{
			for each (var eff:Effect in effects) {
				eff.destroy();
			}
			effects = null;
			
			addExplosion();
			
			clock.stop();
			clock.removeEventListener(TimerEvent.TIMER, heartBeat);
			
			removeEventListener(Event.ENTER_FRAME, enterFrame);
			
			Collider.instance.remove(this);
			
			removeChildren(0, -1, true);
			
			this.removeFromParent(true);
		}
		
		public function onCollide(box:Box) :void {
			/*if (referenceBox != null) {
				referenceBox.onCollide(box);
			}*/
			
			trace("[", name, " (", id, ", group: ", group,")] collides with ", box.name, "(", box.id, ", group: ", box.group, ") dmg:", box.dmg);
			//trace("1.", collisionBox, "2.", box.collisionBox);
			
			if (!damaged){
				applyDamage(box.dmg);
				damaged = true;
			}
		}
		
		public function onAttack(victim:Box):void {
			
		}
		
		private function enterFrame(e:Event):void 	{
			onUpdate();
		}
		
		public function onUpdate():void {
			collisionBox.x = x// + pivotX;
			collisionBox.y = y// + pivotY;
			pos.setTo(x, y);
		}
		
	}

}