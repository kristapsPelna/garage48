package objects 
{
	import flash.geom.Point;
	import gui.GUI;
	import gui.YouLoseWindow;
	import mechanics.Arsenal;
	import mechanics.Controller;
	import mechanics.Weapon;
	import monster.Boss;
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.utils.Color;
	import util.Fades;
	import util.Media;
	import util.Notification;
	import util.NotificationCenter;
	import util.UtilLib;
	/**
	 * ...
	 * @author Ace
	 */
	public class Character extends Box
	{
		private static var _instance:Character;
		
		public static function get instance():Character
		{
			return _instance;
		}
		
		private var movieClip:MovieClip;
		private var weaponMC:MovieClip;
		
		private const idle:String = "heroidlefront";
		private const idleBack:String = "heroidleback";
		private const idleRight:String = "heroidleside";
		private const idleLeft:String = "idleLeft";
		
		private const move:String = "herofrontwalk";
		private const moveBack:String = "herobackwalk";
		private const moveRight:String = "herosidewalk";
		private const moveLeft:String = "moveLeft";
		
		private var currentState:String = idle;
		
		public var aimPoint:Point = new Point(x, y);
		
		private var weapon:Weapon;
		
		private var standing:Boolean = true;
		
		public function Character() 
		{
			_instance = this;
			
			hp = maxHP = 1000;
			weapon = Arsenal.getWeapon(1);
			weapon.owner = this;
			walkThrough = true;
			
			group = "player";
			name = "player";
		}
		
		override public function create():void 
		{
			super.create();
			//setAnimation( new Animation(0, 2) );
			
			changeMovieClip();
			
			//alignPivot();// HAlign.CENTER, VAlign.BOTTOM);
		}
		
		override public function onUpdate():void 
		{
			super.onUpdate();
			followMouse();
		}
		
		private function followMouse():void 
		{
			//trace("mouse", Starling.current.nativeStage.mouseX, Starling.current.nativeStage.mouseY);
			var angle:int = UtilLib.angleBetween(this.x, this.y, Starling.current.nativeStage.mouseX, Starling.current.nativeStage.mouseY) + 180;
			
			var leftOver:int = angle % 90;
			
			if (leftOver < 45)
				angle -= leftOver;
			else
				angle += 90 - leftOver;
			
			angle = angle % 360;
			
			switch (angle)
			{
				case 270:
					if (standing)
						setState(idle);
					else
						setState(move);
					break;
				case 180:
					if (standing)
						setState(idleRight);
					else
						setState(moveRight);
					break;
				case 90:
					if (standing)
						setState(idleBack);
					else
						setState(moveBack);
					break;
				case 0:
					if (standing)
						setState(idleLeft);
					else
						setState(moveLeft);
					break;
			}
			//changeMovieClip();
		}
		
		private function changeMovieClip():void
		{
			Starling.juggler.remove(movieClip);
			removeChild(movieClip);
			
			if (currentState == idleLeft || currentState == moveLeft)
			{
				if (currentState == idleLeft)
					movieClip = new MovieClip(Media.instance.getTextures(idleRight), 24);
				else if (currentState == moveLeft)
					movieClip = new MovieClip(Media.instance.getTextures(moveRight), 24);
					
				movieClip.scaleX = -1;
				movieClip.x = movieClip.width;
			}
			else
			{
				movieClip = new MovieClip(Media.instance.getTextures(currentState), 24);
				
				movieClip.scaleX = 1;
				movieClip.x = 0;
			}
			
			Starling.juggler.add(movieClip);
			addChild(movieClip);
			
			showWeapon();
		}
		
		private function showWeapon():void 
		{
			if (weaponMC)
				removeChild(weaponMC);
				
			if (currentState == moveBack || currentState == idleBack)
			{
				aimPoint.x = width * 0.4;
				aimPoint.y = height * 0.5;
				return;
			}
				
			weaponMC = new MovieClip(Media.instance.getTextures("pudele"), 12);
			weaponMC.currentFrame = 1;
			
			weaponMC.x = movieClip.width * 0.5 - weaponMC.width * 0.5;
			weaponMC.y = movieClip.height * 0.5 - weaponMC.height * 0.5;
			weaponMC.scaleX = 1;
			weaponMC.rotation = 0;
			
			switch (currentState)
			{
				case moveLeft:
				case idleLeft:
					weaponMC.scaleX = -1;
					weaponMC.x = movieClip.width * 0.7 - weaponMC.width * 0.5;
					weaponMC.y = movieClip.height * 0.6 - weaponMC.height * 0.5;
					
					aimPoint.x = weaponMC.x - weaponMC.width;
					aimPoint.y = weaponMC.y + weaponMC.height * 0.5;
					break;
				case moveRight:
				case idleRight:
					weaponMC.scaleX = 1;
					weaponMC.x = movieClip.width - weaponMC.width * 0.5;
					weaponMC.y = movieClip.height * 0.6 - weaponMC.height * 0.5;
					
					aimPoint.x = weaponMC.x + weaponMC.width;
					aimPoint.y = weaponMC.y + weaponMC.height * 0.5;
					break;
				case move:
				case idle:
					weaponMC.rotation = UtilLib.radians(90);
					weaponMC.x = movieClip.width * 0.8 - weaponMC.width * 0.5;
					weaponMC.y = movieClip.height * 0.75 - weaponMC.height * 0.5;
					
					aimPoint.x = weaponMC.x + weaponMC.width * 0.5;
					aimPoint.y = weaponMC.y + weaponMC.height;
					break;
			}
			
			addChild(weaponMC);
		}
		
		public function stopMovement():void
		{
			standing = true;
			/*switch (currentState)
			{
				case move:
					setState(idle);
					break;
				case moveBack:
					setState(idleBack);
					break;
				case moveRight:
					setState(idleRight);
					break;
				case moveLeft:
					setState(idleLeft);
					break;
			}*/
		}
		
		private function setState(state:String):void
		{
			if (currentState != state)
			{
				currentState = state;
				changeMovieClip();
			}
			else
				currentState = state;
		}
		
		override public function moveInDirection(dirX:int, dirY:int):void
		{
			super.moveInDirection(dirX, dirY);
			
			standing = false;
			//trace("test", dirX, dirY);
			/*if (Math.abs(dirY) > Math.abs(dirX))
			{
				if (dirY < 0)
					setState(moveBack);
				else// if (dirY > 0)
					setState(move);
			}
			else
			{
				if (dirX < 0)
					setState(moveLeft);
				else// if (dirX > 0)
					setState(moveRight);
			}*/
		}
		
		override public function reset():void 
		{
			super.reset();
			
			setHP(maxHP);
			setStamina(maxStamina);
			
		}
		
		override public function destroy():void 	{
			super.destroy();
			removeEventListeners();
			if (weapon) {
				weapon.owner = null;
				weapon = null;
			}
			Controller.instance.object = null;
			Controller.instance.destroy();
			
			Game.instance.endGame(false);
		}
			
		override public function setHP(hp:Number):void 
		{
			super.setHP(hp);
			NotificationCenter.post(Notification.healthChange);
		}
		
		override public function applyDamage(dmg:Number):void 
		{
			super.applyDamage(dmg);
			blink(Color.RED);
			
		}
		
		public function blink(color:int):void {
			Starling.juggler.tween(movieClip, 0.15, {
				color: color,
				transition: Transitions.LINEAR,
				reverse: true,
				repeatCount: 4
			});
		}
		
		public function shoot(location:Point):void		{
			if (weapon!=null){
				weapon.shoot(location);
			}
		}
		
		override public function onCollide(box:Box):void 
		{
			super.onCollide(box);
			if (box is Boss) {
				// addChild(new Fades(0, -25, "-10 HP", { color: Color.FUCHSIA, totalTime: 3.0, size: 25, floatY: 30} ));
			}
		}
		
	}

}