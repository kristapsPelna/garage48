package objects 
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	/**
	 * ...
	 * @author Ace
	 */
	public class Overlay extends Sprite
	{
		private var image:Image;
		
		[Embed(source="/../assets/light.png")]
		private static const light:Class;
		
		[Embed(source="/../assets/velasmazgatuvegaismas.png")]
		private static const light2:Class;
		
		private var textures:Vector.<Texture>;
		
		public function Overlay(id:int = 0) 
		{
			super();
			
			textures = new Vector.<Texture>();
			textures.push( Texture.fromBitmapData(new light().bitmapData) );
			textures.push( Texture.fromBitmapData(new light2().bitmapData) );
			
			image = new Image(textures[0]);
			addChild(image);
			
			show(id);
		}
		
		public function show(id:int = 0):void {
			
			switch (id) {
				case 0:
					image.texture = textures[0];
					image.alignPivot(HAlign.LEFT, VAlign.TOP);
					image.x = 0;
					image.y = 0;
					break;
				case 1:
					image.texture = textures[1];
					image.alignPivot(HAlign.RIGHT, VAlign.BOTTOM);
					image.x = Constants.stageW;
					image.y = Constants.stageH;
					break;
			}
		}
		
	}

}