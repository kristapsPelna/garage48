package objects 
{
	import mechanics.Collider;
	import mechanics.Controller;
	import mechanics.MonsterBoxLibrary;
	import monster.Boss;
	import monster.MonsterSelector;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	import util.MonsterSortMC;
	/**
	 * ...
	 * @author Ace
	 */
	public class Room extends Sprite
	{
		private var overlay:Overlay;
		private var background:Background;
		private var ground:Sprite;
		
		private var collider:Collider;
		
		private var wallVector:Vector.<Wall>;
		
		private var player:Character;
		
		private static var _instance:Room;
		private var BOSS:Boss;
		private var type:int;
		
		public var level:int = 1;
		
		public static function get instance():Room
		{
			return _instance;
		}
		
		public function Room() 
		{
			_instance = this;
			
			type = Constants.random.nextMinMax(0, 2);
			
			background = new Background(  );
			background.show(type);
			addChild(background);
			
			ground = new Sprite();
			addChild(ground);
			
			collider = new Collider();
			
			//addWalls();
			//addMonster();
			//addPlayer();
			
			
			overlay = new Overlay(type);
			addChild(overlay);
			
			reset();
		}
		
		public function reset():void {
			
			Starling.juggler.purge();
			
			Game.instance.waitingForVictory = false;
			
			Collider.instance.reset();
			ground.removeChildren(0, -1, true);
			/*var i:int = 0;
			var count: int = numChildren;
			var child:DisplayObject;
			for (i = 0; i < count; i++) {
				//child = 
			}*/
			
			type = Constants.random.nextMinMax(0, 2);
			
			background.show(type);
			overlay.show(type);
			
			//addWalls();
			
			if (player) {
				player.destroy();
			}
			
			addPlayer();
			
			if (BOSS) {
				BOSS.destroy();
			}
			addMonster();
			
			
			Game.instance.waitingForVictory = true;
			
			Starling.juggler.delayCall(BOSS.activate, 5.0);
		}
		
		private function addPlayer():void 
		{
			player = new Character();
			player.x = Constants.stageW * 0.5;
			player.y = Constants.stageH * 0.75;
			collider.add(player);
			ground.addChild(player);
			MonsterSortMC.swapMC(player);
			
			new Controller(player);
		}
		
		private function addMonster():void
		{
			BOSS = new Boss();
			MonsterSelector.selectBoss(BOSS);
			BOSS.name = "BOSS";
			BOSS.x = 110;
			BOSS.y = 150;
			collider.add(BOSS);
			ground.addChild(BOSS);	
			MonsterSortMC.swapMC(BOSS);
		}
		
		private function addWalls():void 
		{
			wallVector = new <Wall>[];
			
			const leftWall:int = Constants.stageW * 0.075;
			const rightWall:int = Constants.stageW * 0.078;
			const upperWall:int = Constants.stageW * 0.01;
			const lowerWall:int = Constants.stageH * 0.09;
			
			var wall:Wall = new Wall(leftWall, Constants.stageH);
			wallVector.push(wall);
			
			wall = new Wall(rightWall, Constants.stageH);
			wall.x = Constants.stageW - wall.width;
			wallVector.push(wall);
			
			wall = new Wall(Constants.stageW - leftWall - rightWall, upperWall);
			wall.x = leftWall;
			wallVector.push(wall);
			
			wall = new Wall(Constants.stageW - leftWall - rightWall, lowerWall);
			wall.x = leftWall;
			wall.y = Constants.stageH - lowerWall;
			wallVector.push(wall);
			
			for each (wall in wallVector)
			{
				collider.addWall(wall);
				ground.addChild(wall);
			}
		}
		
		public function add(obj:DisplayObject):void {
			ground.addChild(obj);
		}
		
	}

}