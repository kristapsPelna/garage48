package  
{
	import com.greensock.motionPaths.RectanglePath2D;
	import flash.geom.Rectangle;
	import util.RandomSeed;
	/**
	 * ...
	 * @author Ace
	 */
	public class Constants 
	{
		public static var stageW:int = 1024;
		public static var stageH:int = 768;
		public static const screenRect:Rectangle = new Rectangle(0, 0, stageW, stageH);
		
		public static var ratio:Number = stageW / stageH;
		
		public static var random:RandomSeed = new RandomSeed(Math.random()*1000);
		
		public function Constants() 
		{
			
		}
		
	}

}